// Mozilla User Preferences

// DO NOT EDIT THIS FILE.
//
// If you make changes to this file while the application is running,
// the changes will be overwritten when the application exits.
//
// To change a preference value, you can either:
// - modify it via the UI (e.g. via about:config in the browser); or
// - set it within a user.js file in your profile.

user_pref("app.normandy.first_run", false);
user_pref("app.update.auto", false);
user_pref("app.update.lastUpdateTime.addon-background-update-timer", 0);
user_pref("app.update.lastUpdateTime.background-update-timer", 0);
user_pref("app.update.lastUpdateTime.blocklist-background-update-timer", 0);
user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 1535974480);
user_pref("app.update.lastUpdateTime.recipe-client-addon-run", 0);
user_pref("app.update.lastUpdateTime.search-engine-update-timer", 1535974600);
user_pref("app.update.lastUpdateTime.telemetry_modules_ping", 0);
user_pref("app.update.lastUpdateTime.xpi-signature-verification", 0);
user_pref("browser.cache.disk.capacity", 358400);
user_pref("browser.cache.disk.enable", false);
user_pref("browser.cache.disk.filesystem_reported", 1);
user_pref("browser.cache.disk.smart_size.first_run", false);
user_pref("browser.download.useDownloadDir", false);
user_pref("browser.laterrun.bookkeeping.profileCreationTime", 1535974449);
user_pref("browser.laterrun.bookkeeping.sessionCount", 3);
user_pref("browser.laterrun.enabled", true);
user_pref("browser.migration.version", 68);
user_pref("browser.newtabpage.activity-stream.impressionId", "{147c65a7-1d1c-4152-8941-d993e0b65668}");
user_pref("browser.newtabpage.activity-stream.migrationExpired", true);
user_pref("browser.newtabpage.activity-stream.migrationLastShownDate", 1535922000);
user_pref("browser.newtabpage.activity-stream.migrationRemainingDays", 3);
user_pref("browser.newtabpage.blocked", "{\"T9nJot5PurhJSy8n038xGA==\":1,\"K00ILysCaEq8+bEqV/3nuw==\":1,\"4gPpjkxgZzXPVtuEoAL9Ig==\":1,\"eV8/WsSLxHadrTL1gAxhug==\":1,\"26UbzFJ7qT9/4DhodHKA1Q==\":1,\"gLv0ja2RYVgxKdp0I5qwvA==\":1}");
user_pref("browser.newtabpage.storageVersion", 1);
user_pref("browser.onboarding.notification.finished", true);
user_pref("browser.onboarding.notification.last-time-of-changing-tour-sec", 1535974455);
user_pref("browser.onboarding.seen-tourset-version", 2);
user_pref("browser.onboarding.state", "watermark");
user_pref("browser.onboarding.tour-type", "new");
user_pref("browser.onboarding.tour.onboarding-tour-addons.completed", true);
user_pref("browser.onboarding.tour.onboarding-tour-customize.completed", true);
user_pref("browser.onboarding.tour.onboarding-tour-default-browser.completed", true);
user_pref("browser.onboarding.tour.onboarding-tour-performance.completed", true);
user_pref("browser.onboarding.tour.onboarding-tour-private-browsing.completed", true);
user_pref("browser.onboarding.tour.onboarding-tour-screenshots.completed", true);
user_pref("browser.pageActions.persistedActions", "{\"version\":1,\"ids\":[\"bookmark\",\"bookmarkSeparator\",\"copyURL\",\"emailLink\",\"addSearchEngine\",\"sendToDevice\",\"pocket\",\"screenshots\"],\"idsInUrlbar\":[\"pocket\",\"bookmark\"]}");
user_pref("browser.pagethumbnails.storage_version", 3);
user_pref("browser.places.importBookmarksHTML", false);
user_pref("browser.places.smartBookmarksVersion", 8);
user_pref("browser.sessionstore.upgradeBackup.latestBuildID", "20180807170231");
user_pref("browser.shell.checkDefaultBrowser", false);
user_pref("browser.slowStartup.averageTime", 684);
user_pref("browser.slowStartup.samples", 3);
user_pref("browser.startup.homepage_override.buildID", "20180807170231");
user_pref("browser.startup.homepage_override.mstone", "61.0.2");
user_pref("browser.startup.page", 3);
user_pref("browser.uiCustomization.state", "{\"placements\":{\"widget-overflow-fixed-list\":[],\"PersonalToolbar\":[\"personal-bookmarks\"],\"nav-bar\":[\"back-button\",\"forward-button\",\"stop-reload-button\",\"home-button\",\"customizableui-special-spring1\",\"urlbar-container\",\"customizableui-special-spring2\",\"downloads-button\",\"library-button\",\"sidebar-button\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"]},\"seen\":[\"developer-button\",\"webide-button\"],\"dirtyAreaCache\":[\"PersonalToolbar\",\"nav-bar\",\"toolbar-menubar\",\"TabsToolbar\"],\"currentVersion\":14,\"newElementCount\":2}");
user_pref("browser.urlbar.placeholderName", "Google");
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionPolicyAcceptedVersion", 2);
user_pref("datareporting.policy.dataSubmissionPolicyNotifiedTime", "1535974655181");
user_pref("devtools.onboarding.telemetry.logged", true);
user_pref("distribution.iniFile.exists.appversion", "61.0.2");
user_pref("distribution.iniFile.exists.value", false);
user_pref("extensions.blocklist.pingCountVersion", 0);
user_pref("extensions.databaseSchema", 26);
user_pref("extensions.getAddons.databaseSchema", 5);
user_pref("extensions.lastAppBuildId", "20180807170231");
user_pref("extensions.lastAppVersion", "61.0.2");
user_pref("extensions.lastPlatformVersion", "61.0.2");
user_pref("extensions.pendingOperations", false);
user_pref("extensions.systemAddonSet", "{\"schema\":1,\"addons\":{}}");
user_pref("extensions.ui.dictionary.hidden", true);
user_pref("extensions.ui.lastCategory", "addons://list/extension");
user_pref("extensions.ui.locale.hidden", true);
user_pref("extensions.webextensions.uuids", "{\"webcompat@mozilla.org\":\"47ac8c5b-9bbe-4777-9f2d-c8e33fe3f8a7\",\"screenshots@mozilla.org\":\"af86b21a-317c-412b-96cd-5a44db198875\"}");
user_pref("layers.mlgpu.sanity-test-failed", false);
user_pref("lightweightThemes.usedThemes", "[]");
user_pref("media.gmp.storage.version.observed", 1);
user_pref("media.hardware-video-decoding.failed", false);
user_pref("network.cookie.prefsMigrated", true);
user_pref("network.predictor.cleaned-up", true);
user_pref("pdfjs.enabledCache.initialized", true);
user_pref("pdfjs.enabledCache.state", true);
user_pref("pdfjs.migrationVersion", 2);
user_pref("pdfjs.previousHandler.alwaysAskBeforeHandling", true);
user_pref("places.history.expiration.transient_current_max_pages", 104858);
user_pref("plugin.disable_full_page_plugin_for_types", "application/pdf");
user_pref("privacy.sanitize.pending", "[]");
user_pref("privacy.sanitize.timeSpan", 0);
user_pref("sanity-test.advanced-layers", true);
user_pref("sanity-test.device-id", "0x1401");
user_pref("sanity-test.driver-version", "22.21.13.8165");
user_pref("sanity-test.running", false);
user_pref("sanity-test.version", "20180807170231");
user_pref("security.sandbox.content.tempDirSuffix", "{a42d9a90-bb71-44ff-9a2f-858a32888e5e}");
user_pref("services.sync.clients.lastSync", "0");
user_pref("services.sync.declinedEngines", "");
user_pref("services.sync.globalScore", 0);
user_pref("services.sync.nextSync", 0);
user_pref("services.sync.tabs.lastSync", "0");
user_pref("signon.importedFromSqlite", true);
user_pref("toolkit.startup.last_success", 1535977088);
user_pref("toolkit.telemetry.cachedClientID", "f2e38fb0-4c34-4686-b37a-66f0c98c66da");
user_pref("toolkit.telemetry.previousBuildID", "20180807170231");
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
