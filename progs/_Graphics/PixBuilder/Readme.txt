PixBuilder Studio 2.00
Copyright (C) 2004 - 2011 by WnSoft Graphics
--------------------------------------------
Web site:           http://www.wnsoft.com
Technical support:  support@wnsoft.com
 
PixBuilder Studio is a powerful and easy to use application for image creation,
viewing and manipulation. The program is ideal for working with digital photos.
Also you can create and open icons. The application run instantly, processes
images up quickly and with high quality. The work with layers is supported,
multi-step undo is available, print with preview and save with preview are
also of use.
Features include resizing, rotating, text operations and many others.
It works under Windows 95, 98, Me, NT, 2000 and XP. The program has 
multilingual interface.

Main features:
-----------------------------------
- Supported formats
  for read  : BMP, PNG, JPG, GIF, TIFF, PCX, TGA, PSD, ICO
  for write : BMP, PNG, JPG, GIF, TIFF, PCX, TGA, ICO
- Work with layers;
- Multi-step undo;
- Levels, curves, color balance, brightness\contrast, rotate etc;
- A big number of effects;
- Save with preview;
- Print with preview;
