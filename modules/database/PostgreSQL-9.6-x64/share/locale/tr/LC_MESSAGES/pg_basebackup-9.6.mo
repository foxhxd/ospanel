��    �      \    �      �     �     �            !   (  
   J  -   U  X   �  T   �  R   1  `   �  I   �  D   /     t  3   �  K   �  <     j   C  >   �  N   �  @   <  3   }  Y   �  >     1   J  j   |  <   �  ?   $  M   d  k   �  J     Y   i  +   �  0   �  B      B   c  *   �  8   �  �   
  5   �  �   �  r   W  o   �  1   :  3   l  K   �  -   �  F     \   a  P   �  2     4   B  8   w  1   �  4   �  1     .   I  @   x  1   �  E   �  B   1   7   t      �      �   (   �   '   !  &   7!  (   ^!  $   �!  -   �!  '   �!  "   "      %"  #   F"  2   j"  ,   �"  +   �"  .   �"  (   %#  #   N#  5   r#  f   �#  ,   $  9   <$  4   v$  7   �$  =   �$  d   !%  A   �%  "   �%  &   �%  &   &  #   9&  /   ]&  >   �&  a   �&  &   .'  &   U'  %   |'  1   �'  8   �'  (   (  2   6(      i(  &   �(  '   �(  .   �(  +   )  #   4)  2   X)  &   �)  &   �)  /   �)  +   	*  4   5*  /   j*  !   �*  1   �*  )   �*  (   +  2   A+  2   t+  3   �+  0   �+  %   ,  #   2,  +   V,     �,     �,  2   �,  #   �,     -  ,   %-  ,   R-  #   -  i   �-  i   .  ?   w.  "   �.  A   �.  #   /      @/  9   a/     �/     �/  !   �/  D   �/  &   70  #   ^0  A   �0  -   �0  D   �0  !   71     Y1     t1  "   �1     �1  9   �1  1   2  D   82  ,   }2     �2  '   �2  M   �2  :   33  8   n3  6   �3     �3  E   �3  z   =4  %   �4  .   �4  2   5  6   @5  #   w5     �5  0   �5  R   �5  ,   86  4   e6  K   �6  @   �6  >   '7  -   f7  ,   �7  '   �7  ,   �7  o   8  k   �8  4   �8  %   '9  (   M9  ;   v9  
   �9  &   �9     �9  /   �9     :  /   +:  �  [:     <     #<     ><     S<  (   h<     �<  C   �<  ^   �<  S   C=  d   �=  f   �=  L   c>  0   �>     �>  ;   �>  =   1?  9   o?  �   �?  O   4@  X   �@  O   �@  4   -A  Z   bA  >   �A  9   �A  �   6B  T   �B  F   C  G   ]C  r   �C  _   D  l   xD  ,   �D  A   E  B   TE  H   �E  2   �E  ;   F  �   OF  @   �F  �   (G  �   �G  |   FH  &   �H  F   �H  Q   1I  9   �I  E   �I  [   J  O   _J  I   �J  :   �J  P   4K  D   �K  @   �K  <   L  !   HL  G   jL  E   �L  H   �L  N   AM  9   �M     �M  *   �M  *   N  +   <N  >   hN  $   �N  "   �N  :   �N  $   *O  %   OO     uO  !   �O  4   �O  )   �O  /   P  :   EP  ,   �P  %   �P  4   �P  n   Q  .   wQ  M   �Q  ?   �Q  7   4R  8   lR  h   �R  >   S  1   MS  +   S  &   �S  $   �S  @   �S  >   8T  k   wT  $   �T  "   U  ,   +U  3   XU  E   �U  1   �U  2   V     7V     VV  #   vV  '   �V  >   �V  2   W  /   4W  (   dW  6   �W  .   �W  0   �W  -   $X  1   RX  <   �X  C   �X  &   Y  9   ,Y  9   fY  9   �Y  ?   �Y  :   Z  &   UZ  +   |Z  )   �Z     �Z     �Z  B   [  &   G[  *   n[  5   �[  8   �[      \  s   )\  s   �\  G   ]  +   Y]  H   �]  +   �]  $   �]  A   ^  #   a^     �^  $   �^  S   �^  +   _  '   B_  L   j_  0   �_  A   �_     *`  &   G`     n`     �`  &   �`  ,   �`  @    a  A   Aa  3   �a     �a  6   �a  ^   b  C   bb  C   �b  =   �b  #   (c  U   Lc  |   �c  -   d  >   Md  H   �d  9   �d  -   e     =e  D   Ze  U   �e  /   �e  6   %f  =   \f  6   �f  9   �f  9   g  4   Eg  2   zg  /   �g  r   �g  �   Ph  4   �h  0   i  7   8i  D   pi     �i  A   �i      j  -   j     <j  A   Mj     
          C   n   q   �   �       �           �      h   g       �   �             �   ^   ;      �   5       �   (   E   	   )       l   8       &          {   �          �   k   �   �   y   �   2   �   b       �   <       �   �   4   �   S   �   �              �       r       7   �      m   �   _   '       a   L   �       6       �   ,       �   >       �              �   w   z           �   �   9   R   �       3   �   /   +   �                     �   #   c               �   e   s   J      ~       i   %   �   �   }      �           G   ?          ]   X               �   �   `                   �   o       �       A   �   P   �      F   @       �   �   �   Q          �   V   �      f   [       v   Z   H           �   !   :       N       p   �      Y   �   �          �   1      �   .   �      �   =   �   T   x   \   �       �       t   I   �   �   �   �           M                  �                 �      �       0              D   u   �       d   K   $      W          �   j       *   |   �   �   -   B   O       "   �   U    
Action to be performed:
 
Connection options:
 
General options:
 
Optional actions:
 
Options controlling the output:
 
Options:
 
Report bugs to <pgsql-bugs@postgresql.org>.
       --create-slot      create a new replication slot (for the slot's name see --slot)
       --drop-slot        drop the replication slot (for the slot's name see --slot)
       --if-not-exists    do not error if slot already exists when creating a slot
       --start            start streaming in a replication slot (for the slot's name see --slot)
       --synchronous      flush transaction log immediately after writing
       --xlogdir=XLOGDIR  location for the transaction log directory
   %s [OPTION]...
   -?, --help             show this help, then exit
   -D, --directory=DIR    receive transaction log files into this directory
   -D, --pgdata=DIRECTORY receive base backup into directory
   -F  --fsync-interval=SECS
                         time between fsyncs to the output file (default: %d)
   -F, --format=p|t       output format (plain (default), tar)
   -I, --startpos=LSN     where in an existing slot should the streaming start
   -P, --plugin=PLUGIN    use output plugin PLUGIN (default: %s)
   -P, --progress         show progress information
   -R, --write-recovery-conf
                         write recovery.conf for replication
   -S, --slot=SLOTNAME    name of the logical replication slot
   -S, --slot=SLOTNAME    replication slot to use
   -T, --tablespace-mapping=OLDDIR=NEWDIR
                         relocate tablespace in OLDDIR to NEWDIR
   -U, --username=NAME    connect as specified database user
   -V, --version          output version information, then exit
   -W, --password         force password prompt (should happen automatically)
   -X, --xlog-method=fetch|stream
                         include required WAL files with specified method
   -Z, --compress=0-9     compress tar output with given compression level
   -c, --checkpoint=fast|spread
                         set fast or spread checkpointing
   -d, --dbname=CONNSTR   connection string
   -d, --dbname=DBNAME    database to connect to
   -f, --file=FILE        receive log into this file, - for stdout
   -h, --host=HOSTNAME    database server host or socket directory
   -l, --label=LABEL      set backup label
   -n, --no-loop          do not loop on connection lost
   -o, --option=NAME[=VALUE]
                         pass option NAME with optional value VALUE to the
                         output plugin
   -p, --port=PORT        database server port number
   -r, --max-rate=RATE    maximum transfer rate to transfer data directory
                         (in kB/s, or use suffix "k" or "M")
   -s, --status-interval=INTERVAL
                         time between status packets sent to server (in seconds)
   -s, --status-interval=SECS
                         time between status packets sent to server (default: %d)
   -v, --verbose          output verbose messages
   -w, --no-password      never prompt for password
   -x, --xlog             include required WAL files in backup (fetch mode)
   -z, --gzip             compress tar output
 %*s/%s kB (%d%%), %d/%d tablespace %*s/%s kB (%d%%), %d/%d tablespaces %*s/%s kB (%d%%), %d/%d tablespace (%s%-*.*s) %*s/%s kB (%d%%), %d/%d tablespaces (%s%-*.*s) %*s/%s kB (100%%), %d/%d tablespace %*s %*s/%s kB (100%%), %d/%d tablespaces %*s %s controls PostgreSQL logical decoding streams.

 %s receives PostgreSQL streaming transaction logs.

 %s takes a base backup of a running PostgreSQL server.

 %s: %s needs a slot to be specified using --slot
 %s: COPY stream ended before last file was finished
 %s: WAL streaming can only be used in plain mode
 %s: at least one action needs to be specified
 %s: can only write single tablespace to stdout, database has %d
 %s: cannot specify both --xlog and --xlog-method
 %s: cannot use --create-slot or --drop-slot together with --startpos
 %s: cannot use --create-slot or --start together with --drop-slot
 %s: cannot use --create-slot together with --drop-slot
 %s: checkpoint completed
 %s: child %d died, expected %d
 %s: child process did not exit normally
 %s: child process exited with error %d
 %s: child thread exited with error %u
 %s: could not access directory "%s": %s
 %s: could not clear search_path: %s
 %s: could not close compressed file "%s": %s
 %s: could not close directory "%s": %s
 %s: could not close file "%s": %s
 %s: could not connect to server
 %s: could not connect to server: %s %s: could not create archive status file "%s": %s
 %s: could not create background process: %s
 %s: could not create background thread: %s
 %s: could not create compressed file "%s": %s
 %s: could not create directory "%s": %s
 %s: could not create file "%s": %s
 %s: could not create pipe for background process: %s
 %s: could not create replication slot "%s": got %d rows and %d fields, expected %d rows and %d fields
 %s: could not create symbolic link "%s": %s
 %s: could not create symbolic link from "%s" to "%s": %s
 %s: could not create timeline history file "%s": %s
 %s: could not determine seek position in file "%s": %s
 %s: could not determine server setting for integer_datetimes
 %s: could not drop replication slot "%s": got %d rows and %d fields, expected %d rows and %d fields
 %s: could not establish database-specific replication connection
 %s: could not fsync file "%s": %s
 %s: could not fsync log file "%s": %s
 %s: could not get COPY data stream: %s %s: could not get backup header: %s %s: could not get child thread exit status: %s
 %s: could not get transaction log end position from server: %s %s: could not identify system: got %d rows and %d fields, expected %d rows and %d or more fields
 %s: could not initiate base backup: %s %s: could not open directory "%s": %s
 %s: could not open log file "%s": %s
 %s: could not open transaction log file "%s": %s
 %s: could not parse next timeline's starting point "%s"
 %s: could not parse start position "%s"
 %s: could not parse transaction log location "%s"
 %s: could not read COPY data: %s %s: could not read directory "%s": %s
 %s: could not read from ready pipe: %s
 %s: could not receive data from WAL stream: %s %s: could not rename file "%s" to "%s": %s
 %s: could not rename file "%s": %s
 %s: could not send command to background pipe: %s
 %s: could not send copy-end packet: %s %s: could not send feedback packet: %s %s: could not send replication command "%s": %s %s: could not set compression level %d: %s
 %s: could not set permissions on directory "%s": %s
 %s: could not set permissions on file "%s": %s
 %s: could not stat file "%s": %s
 %s: could not stat transaction log file "%s": %s
 %s: could not wait for child process: %s
 %s: could not wait for child thread: %s
 %s: could not write %u bytes to WAL file "%s": %s
 %s: could not write %u bytes to log file "%s": %s
 %s: could not write timeline history file "%s": %s
 %s: could not write to compressed file "%s": %s
 %s: could not write to file "%s": %s
 %s: creating replication slot "%s"
 %s: directory "%s" exists but is not empty
 %s: directory name too long
 %s: disconnected
 %s: disconnected; waiting %d seconds to try again
 %s: dropping replication slot "%s"
 %s: final receive failed: %s %s: finished segment at %X/%X (timeline %u)
 %s: got WAL data offset %08x, expected %08x
 %s: incompatible server version %s
 %s: incompatible server version %s; client does not support streaming from server versions newer than %s
 %s: incompatible server version %s; client does not support streaming from server versions older than %s
 %s: initiating base backup, waiting for checkpoint to complete
 %s: invalid --max-rate unit: "%s"
 %s: invalid checkpoint argument "%s", must be "fast" or "spread"
 %s: invalid compression level "%s"
 %s: invalid fsync interval "%s"
 %s: invalid output format "%s", must be "plain" or "tar"
 %s: invalid port number "%s"
 %s: invalid socket: %s %s: invalid status interval "%s"
 %s: invalid tablespace mapping format "%s", must be "OLDDIR=NEWDIR"
 %s: invalid tar block header size: %d
 %s: invalid transfer rate "%s": %s
 %s: invalid xlog-method option "%s", must be "fetch" or "stream"
 %s: multiple "=" signs in tablespace mapping
 %s: new directory is not an absolute path in tablespace mapping: %s
 %s: no data returned from server
 %s: no database specified
 %s: no slot specified
 %s: no target directory specified
 %s: no target file specified
 %s: no transaction log end position returned from server
 %s: not renaming "%s%s", segment is not complete
 %s: old directory is not an absolute path in tablespace mapping: %s
 %s: only tar mode backups can be compressed
 %s: out of memory
 %s: received interrupt signal, exiting
 %s: replication connection using slot "%s" is unexpectedly database specific
 %s: replication slots can only be used with WAL streaming
 %s: replication stream was terminated before stop point
 %s: segment file "%s" has incorrect size %d, skipping
 %s: select() failed: %s
 %s: server reported unexpected history file name for timeline %u: %s
 %s: server returned unexpected response to BASE_BACKUP command; got %d rows and %d fields, expected %d rows and %d fields
 %s: starting background WAL receiver
 %s: starting log streaming at %X/%X (slot %s)
 %s: starting log streaming at %X/%X (timeline %u)
 %s: starting timeline %u is not present in the server
 %s: streaming header too small: %d
 %s: streaming initiated
 %s: symlinks are not supported on this platform
 %s: system identifier does not match between base backup and streaming connection
 %s: this build does not support compression
 %s: too many command-line arguments (first is "%s")
 %s: transaction log directory location can only be specified in plain mode
 %s: transaction log directory location must be an absolute path
 %s: transaction log file "%s" has %d bytes, should be 0 or %d
 %s: transfer rate "%s" exceeds integer range
 %s: transfer rate "%s" is not a valid value
 %s: transfer rate "%s" is out of range
 %s: transfer rate must be greater than zero
 %s: unexpected response to TIMELINE_HISTORY command: got %d rows and %d fields, expected %d rows and %d fields
 %s: unexpected result set after end-of-timeline: got %d rows and %d fields, expected %d rows and %d fields
 %s: unexpected termination of replication stream: %s %s: unrecognized link indicator "%c"
 %s: unrecognized streaming header: "%c"
 %s: waiting for background process to finish streaming ...
 Password:  Try "%s --help" for more information.
 Usage:
 cannot duplicate null pointer (internal error)
 out of memory
 transaction log start point: %s on timeline %u
 Project-Id-Version: PostgreSQL 9.2
Report-Msgid-Bugs-To: pgsql-bugs@postgresql.org
POT-Creation-Date: 2018-04-20 21:53+0000
PO-Revision-Date: 2018-04-27 15:34+0300
Last-Translator: Abdullah G. Gülner
Language-Team: Turkish <ceviri@postgresql.org.tr>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.7.1
 
Gerçekleştirilecek eylem:
 
Bağlantı Seçenekleri:
 
Genel seçenekler:
 
Seçimli eylemler:
 
Çıktıyı kontrol eden seçenekler: 
 
Seçenekler:
 
Hataları <pgsql-bugs@postgresql.org> adresine bildirebilirsiniz.
       --create-slot      yeni bir replikasyon slot'u oluştur (slot'u adı için bkz. --slot)
       --drop-slot        replikasyon slot'unu sil (slot'un adı için bkz. --slot)
       --if-not-exists    zaten mevcut olan bir slot oluşturulmaya çalışıldığında hata verme
       --start            bir replikasyon slotunda streaming'i başlat (slotun ismi için bkz. --slot)
       --synchronous      write-ahead logu yazıldıktan hemen sonra temizle
       --xlogdir=XLOGDIR  transaction log dizini
   %s [SEÇENEK]...
   -?, --help             bu yardımı göster, sonra çık
   -D, --directory=DIZIN    write-ahead logları bu dizine al
   -D, --pgdata=DİZİN temel yedeğin alınacağı dizin
   -F  --fsync-interval=SANIYE
                         çıktı dosyasına yapılan fsync işlemleri arasındaki, süre (varsayılan: %d)
   -F, --format=p|t       çıktı formatı(p: düz metin(varsayılan), t: tar)
   -I, --startpos=LSN     mevcut bir slot'ta streaming işşleminin başlayacağı konum
   -P, --plugin=EKLENTI    EKLENTI çıktı eklentisini kullan(varsayılan: %s)
   -P, --progress         ilerleme bilgisini göster
   -R, --write-recovery-conf
                         replikasyon için recovery.conf yaz

   -S, --slot=SLOTADI    mantıksal replikasyon slot'unun adı
   -S, --slot=SLOTADI    kullanılacak replikasyon slot'u
   -T, --tablespace-mapping=ESKİDİZİN=YENİDİZİN
                         ESKİDİZİN'de bulunan tablespace'i YENİDİZİN'e taşır
   -U, --username=KULLANICI_ADI    belirtilen veritabanı kullanıcısı ile bağlan
   -V, --version          sürüm bilgisini gösterir ve sonra çıkar
   -W, --password         parola sor (otomatik olarak her zaman açık)
   -X, --xlog-method=fetch|stream
                         gerekli WAL dosyalarını belirtilen yöntemle dahil et
   -Z, --compress=0-9     tar çıktısını belirtilen sıkıştırma seviyesinde sıkıştır
   -c, --checkpoint=fast|spread
                         checkpoint işlemini fast veya spread olarak ayarla
   -d, --dbname=CONNSTR   bağlantı dizgesi
   -d, --dbname=VERITABANI_ADI    bağlanılacak veritabanı adı
   -f, --file=DOSYAADI        logu bu dosyaya al, stdout için -
 
   -h, --host=HOSTNAME    veritabanı sunucusu adresi ya da soket dizini
   -l, --label=ETİKET      yedek etiketini ayarla
   -n, --no-loop          bağlantı sunucusunda loop yapma
   -o, --option=NAME[=VALUE]
                         çıktı eklentisine NAME seçeneğini VALUE opsiyonel
                         değeriyle geçir
   -p, --port=PORT        veritabanı sunucusunun port numarası
   -r, --max-rate=RATE    veri dizinini aktarmak için azami aktarım hızı
                         (kB/s olarak, veya  "k" veya "M" son ekini kullanın)
   -s, --status-interval=INTERVAL
                         sunucuya gönderilen durum paketleri arasındaki zaman (saniye olarak)
   -s, --status-interval=SECS
                         sunucuya yollanan durum paketleri arasındaki süre (varsayılan: %d)
   -v, --verbose          verbose modu
   -w, --no-password      bağlanmak için hiç bir zaman parola sorma
   -x, --xlog             gerekli WAL dosyalarını yedeğe dahil et (fetch modu)
   -z, --gzip             tar çıktısını sıkıştır
 %*s/%s kB (%d%%), %d/%d tablespace %*s/%s kB (%d%%), %d/%d tablespace %*s/%s kB (%d%%), %d/%d tablespace (%s%-*.*s) %*s/%s kB (%d%%), %d/%d tablespace (%s%-*.*s) %*s/%s kB (100%%), %d/%d tablespace %*s %*s/%s kB (100%%), %d/%d tablespace %*s %s PostgreSQL mantıksal kod çözme akımlarını (stream) kontrol eder
 %s stream eden PostgreSQL write-ahead loglarını alır.

 %s çalışan bir PostgreSQL sunucusunun temel yedeğini (base backup) alır. 

 %s: %s bir slotun --slot kullanılarak tanımlanmasını gerektirir
 %s: COPY akışı son dosyanın tamamlanmasından önce kesildi
 %s: WAL streaming sadece düz metin modunda kullanılabilir
 %s: en az bir eylem belirtilmeli
 %s: stdout'a sadece bir tablespace yazılabilir, veritabanında %d var
 %s: --xlog  ve --xlog-method değerlerinin ikisi birden belirtilemez
 %s: --create slot veya --drop slot --startpos ile beraber kullanılamaz
 %s: --create slot veya --start together --drop slot ile beraber kullanılamaz
 %s: --create-slot ile --drop-slot birlikte kullanılamaz
 %s: checkpoint tamamlandı
 %s: %d alt süreç sonlandı, beklenen %d
 %s: alt süreç normal olarak sonlanmadı
 %s: alt süreç %d hata kodu ile sonlandı
 %s: alt iş parçacığı (thread) %u hata kodu ile sonlandı
 %s: "%s" dizine erişim hatası: %s
 %s: search_path temizlenemedi: %s
 %s: "%s" sıkıştırılmış dosyası kapatılamadı: %s
 %s:  "%s" dizini kapatılamadı: %s
 %s: "%s" dosyası kapatılamadı: %s
 %s: sunucuya bağlanılamadı
 %s: sunucuya bağlanılamadı: %s %s: "%s" arşiv durum dosyası oluşturulamadı: %s
 %s: artalan süreci oluşturulamadı: %s
 %s: artalan iş parçası oluşturulamadı: %s
 %s: "%s" sıkıştırılmış dosyası yaratılamadı: %s
 %s: "%s" dizini oluşturma başarısız: %s
 %s: "%s" dosyası yaratılamadı: %s
 %s: artalan süreci için pipe oluşturulamadı: %s
 %s: "%s" replikasyon slot'u oluşturulamadı: %d satır ve %d alan alındı,%d satır ve %d alan bekleniyordu
 %s: symbolic link "%s" oluşturma hatası: %s
 %s: "%s" dosyasından "%s" dosyasına sembolik bağlantı yaratılamadı: %s
 %s: "%s" zaman çizelgesi geçmiş dosyası yaratılamadı: %s
 %s: "%s" dosyasınde arama pozisyonu belirlenemedi: %s
 %s: integer_datetimes için sunucu ayarı belirlenemedi
 %s: "%s" replikasyon slot'u silinemedi: %d satır ve %d alan alındı,%d satır ve %d alan bekleniyordu
 %s: veritabanına özel replikasyon bağlantısı kurulamadı
 %s: "%s" dosyası fsync işlemi başarısız: %s
 %s: "%s" kayıt dosyası fsync hatası: %s
 %s: COPY very akışı alınamadı: %s %s: yedek başlığı alınamadı %s %s: alt iş parçacığı (thread) bitiş durumu alınamadı %s
 %s: sunucudan transaction log bitiş pozisyonu alınamadı: %s %s: sistem belirlenemedi: %d satır ve %d alan alındı, %d satır ve %d veya daha fazla alan bekleniyordu
 %s: temel yedek başlatılamadı: %s %s:  "%s" dizini açılamadı: %s
 %s: "%s" kayıti dosyası açılamıyor: %s
 %s: "%s" write-ahead log dosyası açılamadı: %s
 %s: sonraki timeline'ın başlama noktası "%s" ayrıştırılamadı
 %s: "%s" başlama pozisyonu ayrıştırılamadı
 %s: "%s" transaction log konumu ayrıştıramadı
 %s: COPY verisi okunamadı: %s %s: "%s" dizini okunamadı: %s
 %s:  ready pipe'dan okunamadı: %s
 %s: WAL stream'den veri alınamadı: %s %s: "%s" dosyasının ismi "%s" olarak değiştirilemedi : %s
 %s: "%s" dosyasının ismi değiştirilemedi : %s
 %s: artalan pipe'ına komut gönderilemedi: %s
 %s: kopya-sonu paketi gönderilemedi: %s %s: geri bildirim (feedback) paketi gönderilemedi: %s %s: "%s" replikasyon komutu gönderilemedi: %s %s: %d:sıkıştırma seviyesi ayarlanamadı %s
 %s: "%s" dizininde izinler ayarlanamadı: %s
 %s: "%s" dosyasının izinleri ayarlanamadı: %s
 %s: "%s" dosyasının durumu görüntülenemiyor (stat): %s
 %s: "%s" wal log dosyasının durumu görüntülenemedi (stat): %s
 %s: alt süreç için beklenemedi: %s
 %s: alt iş parçacığı (thread) için beklenemedi: %s
 %1$s: "%3$s" WAL dosyasına %2$u bayt yazılamadı: %4$s
 %1$s: "%3$s" log dosyasına %2$u bayt yazılamadı: %4$s
 %s: "%s" zaman çizelgesi geçmiş dosyasına yazılamadı: %s
 %s: "%s" sıkıştırılmış dosyasına yazılamadı: %s
 %s: "%s":dosyasına yazılamadı:  %s
 %s: "%s" replikasyon slot'u oluşturuluyor
 %s:  "%s" dizini mevcut, ama boş değil
 %s: dizin adı çok uzun
 %s: bağlantı kesildi
 %s: bağlantı kesildi; tekrar denemek için %d saniye bekleniyor
 %s: "%s" replikasyon slot'u siliniyor
 %s: son alma işlemi başarısız oldu: %s %s: segment %X/%X de bitirildi (%u zaman çizelgesi)
 %s: %08x WAL data offset'i alındı , %08x bekleniyordu
 %s: sunucu sürümü %s uyumsuz
 %s: uyumsuz sunucu sürümü %s; istemci %s den daha yeni sunucu sürümlerinden streaming işlemini desteklemiyor
 %s: uyumsuz sunucu sürümü %s; istemci %s den daha eski sunucu sürümlerinden streaming işlemini desteklemiyor
 %s: temel yedek başlatılıyor, checkpoin'in tamamlanması bekleniyor
 %s: --max-rate için geçersiz birim: "%s"
 %s: geçersiz checkpoint argümanı "%s", "fast" ya da "spread" olmalı
 %s: geçersiz sıkıştırma seviyesi "%s"
 %s: geçersiz fsync aralığı "%s"
 %s: geçersiz çıktı biçimi "%s", "plain" ya da "tar" olmalı
 %s: geçersiz port numarası: "%s"
 %s: geçersiz soket: %s %s: geçersiz durum aralığı "%s"
 %s: geçersiz tablespace eşleme biçimi "%s", "ESKİDİZİN=YENİDİZİN" olmalı
 %s: geçersiz tar blok başlık boyutu: %d
 %s: geçersiz aktarım hızı "%s": %s
 %s: geçersiz xlog-yöntemi seçeneği "%s", "fetch" ya da "stream" olmalı
 %s: tablespace eşlemesinde çoklu "=" işareti
 %s: yeni dizin tablespace eşlemesinde mutlak bir yol değil: %s
 %s: sunucudan veri dönmedi
 %s: hiçbir veri tabanı belirtilmedi
 %s: hiç bir slot belirtilmedi
 %s: hedef dizin belirtilmedi
 %s: hiç bir hedef dosya belirtilmedi
 %s: sunucudan WAL bitiş pozisyonu dönmedi
 %s: "%s%s" isim değişikliği yapılmıyor, segment tam değil
 %s: eski dizin tablespace eşlemesinde mutlak bir yol değil: %s
 %s: sadece tar mod yedekleri sıkıştırılabilir
 %s: yetersiz bellek
 %s: kesme (interrupt) sinyali alındı, çıkılıyor
 %s: "%s" slotunu kullanan replikasyon bağlantısı beklenmedik şekilde veritabanı spesifik
 %s: replikasyon slotları sadece WAL streaming ile kullanılabilir
 %s: durma noktasından önce replikasyon akışı sonlandırıldı
 %s: "%s" segment dosyasının boyutu %d yanlış, atlanıyor
 %s: select() başarısız oldu: %s
 %s: sunucu %u zaman çizelgesi için beklenmeyen geçmiş dosyası adı bildirdi: %s
 %s: sunucu BASE_BACKUP komutuna beklenmedik cevap döndü; %d satır ve %d alan alındı, %d satır ve %d alan bekleniyordu
 %s: arka plan WAL alıcısı başlatılıyor
 %s: log streaming %X/%X konumunda başlatılıyor (%s slot'u)
 %s: log streaming %X/%X konumunda başlatılıyor (%u zaman çizelgesi)
 %s: başlayan zaman çizelgesi %u sunucuda mevcut değil
 %s: streaming üst bilgisi çok küçük: %d
 %s: streaming başlatıldı
 %s: bu platformda sembolik bağlantı (symlink) desteklenmemektedir
 %s: system tanımlayıcısı temel yedek ve streaming bağlantısı ile eşleşmiyor
 %s: bu kurulum sıkıştırmayı desteklemiyor
 %s: Çok fazla komut satırı girdisi var (ilki "%s")
 %s: WAL dizini lokasyonu sadece plain modunda belirtilebilir
 %s: transaction log dizini mutlak bir yol olmalıdır
 %s: "%s" wal log dosyası %d bayt, 0 veya %d olmalıydı
 %s: "%s" aktarım hızı tamsayı aralığını aşıyor
 %s: "%s" aktarım hızı geçerli bir değer değil
 %s: "%s" aktarım hızı sonuç sıra dışıdır
 %s: aktarım hızı sıfırdan büyük olmalı
 %s: TIMELINE_HISTORY komutuna beklenmedik cevap; %d satır ve %d alan alındı, %d satır ve %d alan bekleniyordu
 %s: zaman çizelgesi sonundan sonar beklenmedik sonuç kümesi: %d satır ve %d alan alındı, %d satır ve %d alan bekleniyordu
 %s: replikasyon akışında beklenmeyen sonlanma: %s %s: tanımlanamayan bağlantı göstergesi "%c"
 %s: tanınmayan streraming üst bilgisi (header): "%c"
 %s: artalan sürecinin akımı (streaming) bitirmesi bekleniyor ...
 Parola:  Ayrıntılı bilgi için  "%s --help" komutunu deneyebilirsiniz.
 Kullanımı:
 null pointer duplicate edilemiyor (iç hata)
 bellek yetersiz
 transaction log başlama noktası: %2$u zaman çizelgesinde %1$s
 