��    �     4  �  L      h(     i(     (  X   �(  [   �(  B   G)  )   �)  "   �)  x   �)     P*     V*     h*     �*  A   �*  N   �*  P   -+  I   ~+  3   �+  y   �+  G   v,  C   �,  N   -  K   Q-  v   �-  F   .  >   [.  R   �.  B   �.  E   0/  ?   v/  L   �/  3   0  6   70  Q   n0  T   �0  M   1  ?   c1  �   �1  ?   \2  9   �2  5   �2  :   3  K   G3  B   �3  D   �3  �   4  3   �4  �   �4  ?   R5  D   �5  x   �5  6   P6  Q   �6  O   �6  F   )7  >   p7  5   �7  >   �7  E   $8  A   j8  O   �8  H   �8  B   E9  @   �9  =   �9  O   :  H   W:  ;   �:  ,   �:  4   	;  E   >;  0   �;  =   �;  ,   �;  F    <  D   g<  C   �<  F   �<  8   7=  ?   p=  C   �=  =   �=  4   2>  7   g>  L   �>  =   �>  @   *?  J   k?  <   �?  ,   �?  (    @  5   I@  E   @  T   �@     A  9   A  D   WA  G   �A     �A     �A  ?   B  '   ZB  +   �B  e   �B  !   C  ,   6C  4   cC  6   �C  (   �C  /   �C     (D     FD  $   XD  *   }D  2   �D  B   �D  >   E  J   ]E  P   �E  N   �E  H   HF  -   �F     �F     �F  /   �F     G  4   )G  $   ^G  &   �G  *   �G  #   �G     �G  C   H  A   \H  (   �H     �H     �H     �H     I  =   6I  
   tI  ,   I  �   �I  �   FJ  ,   �J     	K  p   K  e   �K  &   �K     L  `   $L  p   �L  4   �L  X   +M  _   �M  E   �M  1   *N  6   \N  4   �N  *   �N  -   �N  D   !O  .   fO  +   �O  +   �O  )   �O  *   P  :   BP  '   }P  =   �P  )   �P  1   Q     ?Q  @   ZQ  7   �Q  6   �Q  6   
R  <   AR  =   ~R  4   �R  3   �R  b   %S  9   �S  <   �S  #   �S  ,   #T  5   PT      �T      �T     �T     �T  '   �T     U  %   U     9U  /   YU  X   �U  &   �U  0   	V  .   :V  )   iV  )   �V     �V     �V     �V  3   �V  (   &W     OW  )   bW  #   �W      �W     �W  &   �W  K   X  &   _X     �X  !   �X  (   �X  '   �X     Y  #   6Y     ZY  /   zY      �Y     �Y  #   �Y  %   Z     1Z  ,   NZ  $   {Z  $   �Z  $   �Z  6   �Z     ![      9[  B   Z[  �   �[  �   4\     �\  $   �\  <   ]  3   E]  *   y]  '   �]  +   �]  (   �]  (   !^  #   J^  -   n^  �   �^  +   l_  ,   �_  !   �_  &   �_  '   `  #   6`     Z`  "   y`  8   �`  $   �`     �`     a  &   9a  @   `a  5   �a  ,   �a     b  "   b  #   Bb  ,   fb     �b  !   �b  #   �b  *   �b  #   #c  %   Gc  0   mc  &   �c  $   �c     �c  "   	d  =   ,d  #   jd  1   �d     �d     �d     �d  :   �d  )   2e  B   \e  P   �e     �e     f     "f     =f  "   Mf     pf     �f  &   �f  &   �f  %   �f  2   g     Ng     cg  &   g  !   �g  )   �g  #   �g  "   h     9h  r   Jh  <   �h  D   �h  N   ?i  F   �i     �i      �i     j  &   /j  ,   Vj  -   �j     �j  "   �j  /   �j     k     6k  (   Sk  @   |k     �k     �k     �k  P   l     al  >   wl  1   �l  >   �l  /   'm  Y   Wm  >   �m     �m     n  "   )n     Ln  &   in  <   �n     �n  '   �n  (   	o  '   2o     Zo     xo     �o  1   �o  +   �o       p  %   !p  3   Gp     {p     �p  "   �p  ;   �p     q     q  0   ;q     lq  /   �q     �q     �q     �q  .   r  I   ?r  >   �r  D   �r     s     s     9s  7   Ks  9   �s  6   �s  H   �s  @   =t     ~t  3   �t  &   �t  0   �t  6   u  /   Su  (   �u  ,   �u     �u  "   �u     v      %v     Fv  h   Wv  T   �v  �   w  6   �w  L   �w  J   x  9   gx     �x  $   �x  +   �x     y     y     2y     Ky     cy  2   wy     �y  "   �y     �y     �y  #   z     +z     Iz  /   `z     �z  &   �z     �z     �z  #   �z     {  $   &{  )   K{      u{  !   �{  %   �{  +   �{     
|  &   *|  '   Q|     y|     �|  0   �|  .   �|  )   }  +   @}     l}     �}  4   �}  �   �}  #   ^~  *   �~     �~     �~     �~     �~  (        7  "   N     q  E   �  #   �     �     �      �  ;   '�     c�     y�     ��  '   ��     ʀ  '   �     
�  %   *�     P�     l�  B   ��  0   ́  !   ��  8   �  :   X�     ��     ��  3   ΂  1   �  1   4�  +   f�  $   ��  $   ��  h   ܃  u  E�     ��     ֆ  R   �  R   >�  [   ��  0   �  +   �  �   J�     Ո     ݈  %   �     �  R   3�  Y   ��  _   ��  j   @�  A   ��  �   �  B   ��  C   ʋ  X   �  h   g�  t   Ќ  I   E�  D   ��  [   ԍ  B   0�  F   s�  D   ��  j   ��  K   j�  S   ��  ]   
�  ]   h�  Y   Ɛ  Z    �  �   {�  F   D�  G   ��  C   Ӓ  =   �  e   U�  S   ��  B   �  �   R�  ?   ��  �    �  A   ��  Q   �  |   @�  B   ��  s    �  a   t�  Q   ֗  K   (�  D   t�  A   ��  F   ��  H   B�  @   ��  \   ̙  P   )�  L   z�  Q   ǚ  e   �  e   �  G   �  B   -�  <   p�  W   ��  0   �  D   6�  0   {�  [   ��  N   �  c   W�  b   ��  G   �  :   f�  F   ��  N   �  @   7�  9   x�  h   ��  M   �  G   i�  X   ��  P   
�  9   [�  (   ��  D   ��  B   �  Z   F�     ��  F   ��  F   �  [   2�     ��      ��  C   ̤  (   �  ,   9�  k   f�  '   ҥ  ,   ��  G   '�  F   o�  .   ��  4   �  &   �     A�  $   Y�  #   ~�  >   ��  R   �  D   4�  L   y�  R   ƨ  Q   �  X   k�  A   ĩ     �     &�  /   =�     m�  9   ��  !   ��  %   �  '   	�  !   1�  -   S�  S   ��  Q   ի  %   '�  #   M�     q�     ��  !   ��  g   Ѭ     9�  9   B�  �   |�  �   G�  G   �  !   a�  r   ��  p   ��  3   g�     ��  a   ��  q   �  :   }�  }   ��  l   6�  ?   ��  ?   �  A   #�  ?   e�  5   ��  )   ۳  A   �  2   G�  /   z�  /   ��  -   ڴ  6   �  O   ?�  1   ��  d   ��  +   &�  :   R�     ��  6   ��  -   �  .   �  .   A�  7   p�  4   ��  -   ݷ  +   �  X   7�  :   ��  N   ˸  +   �  :   F�  J   ��  %   ̹      �     �     �  /   *�     Z�  0   i�  ,   ��  -   Ǻ  b   ��  :   X�  ;   ��  =   ϻ  9   �  9   G�  "   ��     ��     ��  A   ¼  3   �     8�  @   N�  0   ��  #   ��      �  3   �  W   9�  3   ��  !   ž  $   �  0   �  *   =�  !   h�     ��  %   ��  2   п  %   �     )�  "   E�  *   h�     ��  *   ��  !   ��  $    �  "   %�  5   H�     ~�  '   ��  @   ��  �   �  �   ��  *   �  #   ?�  ;   c�  8   ��  %   ��  $   ��  (   #�  3   L�  #   ��  +   ��  2   ��  �   �  /   ��  2   �  #   C�  *   g�  -   ��  &   ��  !   ��  !   	�  <   +�  (   h�  #   ��     ��  4   ��  E   �  ;   M�  /   ��  '   ��     ��  "   ��  *   !�  "   L�      o�  )   ��  2   ��  $   ��  -   �  5   @�  :   v�  +   ��  +   ��  !   	�  8   +�  +   d�  +   ��     ��     ��     ��  ,   ��  -   ,�  \   Z�  M   ��     �     �  7   5�     m�  *   ��  0   ��     ��  )   ��  )   )�  (   S�  ;   |�  $   ��  1   ��  6   �  %   F�  -   l�  +   ��  (   ��     ��  �   �  D   ��  \   ��  e   P�  i   ��  +    �  %   L�     r�  0   ��  9   ��  5   ��     /�  !   M�  4   o�     ��     ��  5   ��  @   �     R�     o�      ��  S   ��     �  ^   �  $   |�  4   ��  .   ��  S   �  :   Y�     ��  !   ��  '   ��  !   ��  2    �  P   S�     ��  1   ��  5   ��  4   *�  "   _�     ��     ��  7   ��  7   ��      $�  -   E�  @   s�     ��  !   ��  1   ��  J    �     k�     ��  :   ��     ��  ;   ��  *   2�  #   ]�     ��  :   ��  A   ��  N   �  T   j�     ��  "   ��     ��  =   �  :   F�  8   ��  `   ��  8   �     T�  1   q�  +   ��  *   ��  4   ��  .   /�  "   ^�  ;   ��     ��  &   ��     ��  $   �     5�  �   Q�  S   ��  �   +�  <   ��  c   �  C   |�  <   ��     ��  2   �  +   C�  &   o�     ��  !   ��     ��     ��  E   �     R�  '   e�     ��     ��  0   ��     ��     �  R   �     r�  3   ��     ��     ��  ,   ��     �  3   /�  6   c�  ,   ��  .   ��  1   ��  7   (�  ,   `�  6   ��  2   ��  ,   ��  (   $�  ?   M�  8   ��  A   ��  8   �  &   A�  *   h�  ?   ��  �   ��  %   e�  :   ��  #   ��     ��     ��  &   �  -   D�     r�      ��     ��  a   ��  '   0�     X�     u�     ��  ?   ��     ��     ��     ��  3   �     G�  +   g�  $   ��  -   ��     ��     �  h   #�  7   ��  .   ��  8   ��  9   ,�  !   f�      ��  2   ��  4   ��  5   �  4   G�  #   |�  1   ��  w   ��     d   �   �   �  �       x  �   �  �  �       B  I              �   �  �  )   �          5        |          �   #  �   J       F   /  r  %  A   �   �      �          d  �   �  B   }   G   S  �  �  �       Y   _   �      {   '   �  C  �   a       �      �      =   �   D      �   R   w  @      �      �           �               �      /   |   g  (   >  �  �       X  �   �          �  �       �   j      �   x   n  �     �   �         �   �   �      �   [  �  -   U   �  m   �  ^       �   �       �   	   �   -  �      �   �   M   O   �      �             �   J          �  �          p  ~   `  �   .  s         1     �   f  �  �       �      ;   �  �  D   �   L   �   �   z  �        I   :  �          �   �   �   H           X       l      �   �   �  �          �          �         '             `   �  �   �               �   �      �            Y  �     L  p       t  9   )          �           �   Q   �   �  h       \   �   3                  F  �       E   �  �   �   "     u           	  K  
   �   �   �    &       a  ?      �       �  �  �  �             �   2       o       �   4   �      �     z   �  �  �  y   v      �   q  �  "  K           �   g       �   �         �  �       .   ]  �  A    �  l         �   :   }  e         =  �                Z     ^  o  U  �   Q  �      �     !  n   �   �   W      �       �      �       �       M      *  m  �   �   �   �      �       �  �       >              �       �         �       �   �  �  
  <   �   �       �  �           �   [       �     {  �  6     C       �       �     t   �  �           �  �   �  $  ?   q   �   �  !   �   N       �     e  �        �   N  E  �          i       O      �   w   �       T      9  7         <          �       $   �               c   8           �  �   c      ,      6      �      3  �  �  j   r   �  �  �   �  �          �  #   �      �      �  h  +     �  _  �    �          �  \  �         ;      P      �   �  �       1           i  f   �       �  k  G      �   b  5   W  �      �  H  �      0    P   �   8      �  �       �   2  �              0   �       �  �           �  *   +  �      �       �   �         �  %   ]   �   Z          s   b   (              �         �              ,  �   R  �       �  @   S   V  �      4     ~  �       &  �  �   k   7  �   T   �   y  V   v   u  �         
Connection options:
 
General options:
 
If -f/--file is not used, then the SQL script will be written to the standard
output.

 
If no database name is supplied, then the PGDATABASE environment
variable value is used.

 
If no input file name is supplied, then standard input is used.

 
Options controlling the output content:
 
Options controlling the restore:
 
The options -I, -n, -P, -t, -T, and --section can be combined and specified
multiple times to select multiple objects.
   %s
   %s [OPTION]...
   %s [OPTION]... [DBNAME]
   %s [OPTION]... [FILE]
   --binary-upgrade             for use by upgrade utilities only
   --column-inserts             dump data as INSERT commands with column names
   --disable-dollar-quoting     disable dollar quoting, use SQL standard quoting
   --disable-triggers           disable triggers during data-only restore
   --enable-row-security        enable row security
   --enable-row-security        enable row security (dump only content user has
                               access to)
   --exclude-table-data=TABLE   do NOT dump data for the named table(s)
   --if-exists                  use IF EXISTS when dropping objects
   --inserts                    dump data as INSERT commands, rather than COPY
   --lock-wait-timeout=TIMEOUT  fail after waiting TIMEOUT for a table lock
   --no-data-for-failed-tables  do not restore data of tables that could not be
                               created
   --no-security-labels         do not dump security label assignments
   --no-security-labels         do not restore security labels
   --no-synchronized-snapshots  do not use synchronized snapshots in parallel jobs
   --no-tablespaces             do not dump tablespace assignments
   --no-tablespaces             do not restore tablespace assignments
   --no-unlogged-table-data     do not dump unlogged table data
   --quote-all-identifiers      quote all identifiers, even if not key words
   --role=ROLENAME          do SET ROLE before dump
   --role=ROLENAME          do SET ROLE before restore
   --section=SECTION            dump named section (pre-data, data, or post-data)
   --section=SECTION            restore named section (pre-data, data, or post-data)
   --serializable-deferrable    wait until the dump can run without anomalies
   --snapshot=SNAPSHOT          use given snapshot for the dump
   --use-set-session-authorization
                               use SET SESSION AUTHORIZATION commands instead of
                               ALTER OWNER commands to set ownership
   -1, --single-transaction     restore as a single transaction
   -?, --help                   show this help, then exit
   -?, --help               show this help, then exit
   -C, --create                 create the target database
   -C, --create                 include commands to create database in dump
   -E, --encoding=ENCODING      dump the data in encoding ENCODING
   -F, --format=c|d|t       backup file format (should be automatic)
   -F, --format=c|d|t|p         output file format (custom, directory, tar,
                               plain text (default))
   -I, --index=NAME             restore named index
   -L, --use-list=FILENAME      use table of contents from this file for
                               selecting/ordering output
   -N, --exclude-schema=SCHEMA  do NOT dump the named schema(s)
   -O, --no-owner               skip restoration of object ownership
   -O, --no-owner               skip restoration of object ownership in
                               plain-text format
   -P, --function=NAME(args)    restore named function
   -S, --superuser=NAME         superuser user name to use for disabling triggers
   -S, --superuser=NAME         superuser user name to use in plain-text format
   -S, --superuser=NAME         superuser user name to use in the dump
   -T, --exclude-table=TABLE    do NOT dump the named table(s)
   -T, --trigger=NAME           restore named trigger
   -U, --username=NAME      connect as specified database user
   -V, --version                output version information, then exit
   -V, --version            output version information, then exit
   -W, --password           force password prompt (should happen automatically)
   -Z, --compress=0-9           compression level for compressed formats
   -a, --data-only              dump only the data, not the schema
   -a, --data-only              restore only the data, no schema
   -b, --blobs                  include large objects in dump
   -c, --clean                  clean (drop) database objects before recreating
   -c, --clean                  clean (drop) databases before recreating
   -d, --dbname=CONNSTR     connect using connection string
   -d, --dbname=DBNAME      database to dump
   -d, --dbname=NAME        connect to database name
   -e, --exit-on-error          exit on error, default is to continue
   -f, --file=FILENAME          output file name
   -f, --file=FILENAME          output file or directory name
   -f, --file=FILENAME      output file name
   -g, --globals-only           dump only global objects, no databases
   -h, --host=HOSTNAME      database server host or socket directory
   -j, --jobs=NUM               use this many parallel jobs to dump
   -j, --jobs=NUM               use this many parallel jobs to restore
   -l, --database=DBNAME    alternative default database
   -l, --list               print summarized TOC of the archive
   -n, --schema=NAME            restore only objects in this schema
   -n, --schema=SCHEMA          dump the named schema(s) only
   -o, --oids                   include OIDs in dump
   -p, --port=PORT          database server port number
   -r, --roles-only             dump only roles, no databases or tablespaces
   -s, --schema-only            dump only the schema, no data
   -s, --schema-only            restore only the schema, no data
   -t, --table=NAME             restore named relation (table, view, etc.)
   -t, --table=TABLE            dump the named table(s) only
   -v, --verbose                verbose mode
   -v, --verbose            verbose mode
   -w, --no-password        never prompt for password
   -x, --no-privileges          do not dump privileges (grant/revoke)
   -x, --no-privileges          skip restoration of access privileges (grant/revoke)
 %s %s dumps a database as a text file or to other formats.

 %s extracts a PostgreSQL database cluster into an SQL script file.

 %s restores a PostgreSQL database from an archive created by pg_dump.

 %s: %s    Command was: %s
 %s: WSAStartup failed: %d
 %s: cannot specify both --single-transaction and multiple jobs
 %s: could not connect to database "%s"
 %s: could not connect to database "%s": %s
 %s: could not connect to databases "postgres" or "template1"
Please specify an alternative database.
 %s: could not get server version
 %s: could not open the output file "%s": %s
 %s: could not parse ACL list (%s) for database "%s"
 %s: could not parse ACL list (%s) for tablespace "%s"
 %s: could not parse server version "%s"
 %s: could not re-open the output file "%s": %s
 %s: dumping database "%s"...
 %s: executing %s
 %s: invalid number of parallel jobs
 %s: maximum number of parallel jobs is %d
 %s: option --if-exists requires option -c/--clean
 %s: options -c/--clean and -a/--data-only cannot be used together
 %s: options -d/--dbname and -f/--file cannot be used together
 %s: options -g/--globals-only and -r/--roles-only cannot be used together
 %s: options -g/--globals-only and -t/--tablespaces-only cannot be used together
 %s: options -r/--roles-only and -t/--tablespaces-only cannot be used together
 %s: options -s/--schema-only and -a/--data-only cannot be used together
 %s: pg_dump failed on database "%s", exiting
 %s: query failed: %s %s: query was: %s
 %s: role name starting with "pg_" skipped (%s)
 %s: running "%s"
 %s: too many command-line arguments (first is "%s")
 %s: unrecognized section name: "%s"
 (The INSERT command cannot set OIDs.)
 (The system catalogs might be corrupted.)
 -C and -1 are incompatible options
 COPY failed for table "%s": %s Dumping the contents of table "%s" failed: PQgetCopyData() failed.
 Dumping the contents of table "%s" failed: PQgetResult() failed.
 Error from TOC entry %d; %u %u %s %s %s
 Error message from server: %s Error while FINALIZING:
 Error while INITIALIZING:
 Error while PROCESSING TOC:
 Exported snapshots are not supported by this server version.
 Password:  Report bugs to <pgsql-bugs@postgresql.org>.
 Synchronized snapshots are not supported by this server version.
Run with --no-synchronized-snapshots instead if you do not need
synchronized snapshots.
 Synchronized snapshots are not supported on standby servers.
Run with --no-synchronized-snapshots instead if you do not need
synchronized snapshots.
 TOC Entry %s at %s (length %s, checksum %d)
 The command was: %s
 The program "pg_dump" is needed by %s but was not found in the
same directory as "%s".
Check your installation.
 The program "pg_dump" was found by "%s"
but was not the same version as %s.
Check your installation.
 Try "%s --help" for more information.
 Usage:
 WARNING: aggregate function %s could not be dumped correctly for this database version; ignored
 WARNING: archive is compressed, but this installation does not support compression -- no data will be available
 WARNING: archive items not in correct section order
 WARNING: archive was made on a machine with larger integers, some operations might fail
 WARNING: bogus transform definition, at least one of trffromsql and trftosql should be nonzero
 WARNING: bogus value in pg_cast.castfunc or pg_cast.castmethod field
 WARNING: bogus value in pg_cast.castmethod field
 WARNING: bogus value in pg_transform.trffromsql field
 WARNING: bogus value in pg_transform.trftosql field
 WARNING: bogus value in proargmodes array
 WARNING: could not find operator with OID %s
 WARNING: could not find where to insert IF EXISTS in statement "%s"
 WARNING: could not parse proallargtypes array
 WARNING: could not parse proargmodes array
 WARNING: could not parse proargnames array
 WARNING: could not parse proconfig array
 WARNING: could not parse reloptions array
 WARNING: don't know how to set owner for object type "%s"
 WARNING: errors ignored on restore: %d
 WARNING: ftell mismatch with expected position -- ftell used
 WARNING: invalid creation date in header
 WARNING: invalid type "%c" of access method "%s"
 WARNING: line ignored: %s
 WARNING: owner of aggregate function "%s" appears to be invalid
 WARNING: owner of data type "%s" appears to be invalid
 WARNING: owner of function "%s" appears to be invalid
 WARNING: owner of operator "%s" appears to be invalid
 WARNING: owner of operator class "%s" appears to be invalid
 WARNING: owner of operator family "%s" appears to be invalid
 WARNING: owner of schema "%s" appears to be invalid
 WARNING: owner of table "%s" appears to be invalid
 WARNING: requested compression not available in this installation -- archive will be uncompressed
 WARNING: typtype of data type "%s" appears to be invalid
 WARNING: unexpected extra results during COPY of table "%s"
 a worker process died unexpectedly
 aborting because of server version mismatch
 actual file length (%s) does not match expected (%s)
 allocating AH for %s, format %d
 already connected to a database
 archiver archiver (db) attempting to ascertain archive format
 bad dumpId
 bad table dumpId for TABLE DATA item
 can only reopen input archives
 cannot duplicate null pointer (internal error)
 cannot restore from compressed archive (compression not supported in this installation)
 child process exited with exit code %d child process exited with unrecognized status %d child process was terminated by exception 0x%X child process was terminated by signal %d child process was terminated by signal %s command not executable command not found compress_io compression is not supported by tar archive format
 compression level must be in range 0..9
 compressor active
 connecting to database "%s" as user "%s"
 connecting to database for restore
 connecting to new database "%s"
 connection needs password
 connection to database "%s" failed: %s corrupt tar header found in %s (expected %d, computed %d) file position %s
 could not change directory to "%s": %s could not close TOC file: %s
 could not close archive file: %s
 could not close compression library: %s
 could not close compression stream: %s
 could not close data file: %s
 could not close directory "%s": %s
 could not close input file: %s
 could not close large object TOC file "%s": %s
 could not close output file: %s
 could not close tar member
 could not close temporary file: %s
 could not commit database transaction could not compress data: %s
 could not create communication channels: %s
 could not create directory "%s": %s
 could not create large object %u: %s could not create worker process: %s
 could not determine seek position in archive file: %s
 could not execute query could not find a "%s" to execute could not find block ID %d in archive -- possibly corrupt archive
 could not find block ID %d in archive -- possibly due to out-of-order restore request, which cannot be handled due to lack of data offsets in archive
 could not find block ID %d in archive -- possibly due to out-of-order restore request, which cannot be handled due to non-seekable input file
 could not find entry for ID %d
 could not find file "%s" in archive
 could not find function definition for function with OID %u
 could not find header for file "%s" in tar archive
 could not find parent extension for %s %s
 could not find slot of finished worker
 could not generate temporary file name: %s
 could not get server_version from libpq
 could not identify current directory: %s could not identify dependency loop
 could not initialize compression library: %s
 could not obtain lock on relation "%s"
This usually means that someone requested an ACCESS EXCLUSIVE lock on the table after the pg_dump parent process had gotten the initial ACCESS SHARE lock on the table.
 could not open TOC file "%s" for input: %s
 could not open TOC file "%s" for output: %s
 could not open TOC file "%s": %s
 could not open TOC file for input: %s
 could not open TOC file for output: %s
 could not open input file "%s": %s
 could not open input file: %s
 could not open large object %u: %s could not open large object TOC file "%s" for input: %s
 could not open output file "%s": %s
 could not open output file: %s
 could not open temporary file
 could not parse default ACL list (%s)
 could not parse numeric array "%s": invalid character in number
 could not parse numeric array "%s": too many numbers
 could not parse result of current_schemas()
 could not read binary "%s" could not read directory "%s": %s
 could not read from input file: %s
 could not read from input file: end of file
 could not read input file: %s
 could not read symbolic link "%s" could not reconnect to database: %s could not set default_tablespace to %s: %s could not set default_with_oids: %s could not set search_path to "%s": %s could not set seek position in archive file: %s
 could not set session user to "%s": %s could not start database transaction could not uncompress data: %s
 could not write to blobs TOC file
 could not write to large object (result: %lu, expected: %lu)
 could not write to output file: %s
 could not write to the communication channel: %s
 creating %s "%s"
 creating %s "%s.%s"
 custom archiver definition of view "%s" appears to be empty (length zero)
 did not find magic string in file header
 direct database connections are not supported in pre-1.3 archives
 directory "%s" does not appear to be a valid archive ("toc.dat" does not exist)
 directory archiver directory name too long: "%s"
 disabling triggers for %s
 dropping %s %s
 dumping contents of table "%s.%s"
 enabling triggers for %s
 entering main parallel loop
 entering restore_toc_entries_parallel
 entering restore_toc_entries_postfork
 entering restore_toc_entries_prefork
 entry ID %d out of range -- perhaps a corrupt TOC
 error during backup
 error during file seek: %s
 error processing a parallel work item
 error reading large object %u: %s error reading large object TOC file "%s"
 error returned by PQputCopyData: %s error returned by PQputCopyEnd: %s executing %s %s
 expected %d check constraint on table "%s" but found %d
 expected %d check constraints on table "%s" but found %d
 expected format (%d) differs from format found in file (%d)
 failed sanity check, parent OID %u of table "%s" (OID %u) not found
 failed sanity check, parent table OID %u of pg_rewrite entry OID %u not found
 failed sanity check, parent table OID %u of sequence OID %u not found
 failed to connect to database
 failed to reconnect to database
 file name too long: "%s"
 file offset in dump file is too large
 finding check constraints for table "%s.%s"
 finding default expressions of table "%s.%s"
 finding extension tables
 finding inheritance relationships
 finding the columns and types of table "%s.%s"
 finished item %d %s %s
 finished main parallel loop
 flagging inherited columns in subtables
 found unexpected block ID (%d) when reading data -- expected %d
 function "%s" not found
 identifying extension members
 implied data-only restore
 incomplete tar header found (%lu byte)
 incomplete tar header found (%lu bytes)
 index "%s" not found
 input file appears to be a text format dump. Please use psql.
 input file does not appear to be a valid archive
 input file does not appear to be a valid archive (too short?)
 input file is too short (read %lu, expected 5)
 internal error -- WriteData cannot be called outside the context of a DataDumper routine
 internal error -- neither th nor fh specified in tarReadRaw()
 invalid ENCODING item: %s
 invalid OID for large object
 invalid OID for large object (%u)
 invalid STDSTRINGS item: %s
 invalid adnum value %d for table "%s"
 invalid argument string (%s) for trigger "%s" on table "%s"
 invalid binary "%s" invalid client encoding "%s" specified
 invalid column number %d for table "%s"
 invalid column numbering in table "%s"
 invalid compression code: %d
 invalid dependency %d
 invalid dumpId %d
 invalid line in large object TOC file "%s": "%s"
 invalid message received from worker: "%s"
 invalid number of parallel jobs
 invalid output format "%s" specified
 large-object output not supported in chosen format
 last built-in OID is %u
 launching item %d %s %s
 missing index for constraint "%s"
 moving from position %s to next member at file position %s
 no item ready
 no matching schemas were found
 no matching schemas were found for pattern "%s"
 no matching tables were found
 no matching tables were found for pattern "%s"
 no output directory specified
 not built with zlib support
 now at file position %s
 option --if-exists requires option -c/--clean
 options --inserts/--column-inserts and -o/--oids cannot be used together
 options -c/--clean and -a/--data-only cannot be used together
 options -s/--schema-only and -a/--data-only cannot be used together
 out of memory
 out of on_exit_nicely slots
 parallel archiver parallel backup only supported by the directory format
 parallel restore from non-seekable file is not supported
 parallel restore from standard input is not supported
 parallel restore is not supported with archives made by pre-8.0 pg_dump
 parallel restore is not supported with this archive file format
 pclose failed: %s pgpipe: could not accept connection: error code %d
 pgpipe: could not bind: error code %d
 pgpipe: could not connect socket: error code %d
 pgpipe: could not create second socket: error code %d
 pgpipe: could not create socket: error code %d
 pgpipe: could not listen: error code %d
 pgpipe: getsockname() failed: error code %d
 processing %s
 processing data for table "%s.%s"
 processing item %d %s %s
 processing missed item %d %s %s
 query failed: %s query produced null referenced table name for foreign key trigger "%s" on table "%s" (OID of table: %u)
 query returned %d row instead of one: %s
 query returned %d rows instead of one: %s
 query to get data of sequence "%s" returned %d row (expected 1)
 query to get data of sequence "%s" returned %d rows (expected 1)
 query to get data of sequence "%s" returned name "%s"
 query to get rule "%s" for table "%s" failed: wrong number of rows returned
 query to obtain definition of view "%s" returned more than one definition
 query to obtain definition of view "%s" returned no data
 query was: %s
 read TOC entry %d (ID %d) for %s %s
 reading column info for interesting tables
 reading constraints
 reading default privileges
 reading dependency data
 reading event triggers
 reading extensions
 reading foreign key constraints for table "%s.%s"
 reading indexes
 reading indexes for table "%s.%s"
 reading large objects
 reading policies
 reading policies for table "%s.%s"
 reading procedural languages
 reading rewrite rules
 reading row security enabled for table "%s.%s"
 reading schemas
 reading table inheritance information
 reading transforms
 reading triggers
 reading triggers for table "%s.%s"
 reading type casts
 reading user-defined access methods
 reading user-defined aggregate functions
 reading user-defined collations
 reading user-defined conversions
 reading user-defined foreign servers
 reading user-defined foreign-data wrappers
 reading user-defined functions
 reading user-defined operator classes
 reading user-defined operator families
 reading user-defined operators
 reading user-defined tables
 reading user-defined text search configurations
 reading user-defined text search dictionaries
 reading user-defined text search parsers
 reading user-defined text search templates
 reading user-defined types
 reducing dependencies for %d
 restored %d large object
 restored %d large objects
 restoring data out of order is not supported in this archive format: "%s" is required, but comes before "%s" in the archive file.
 restoring large object with OID %u
 sanity check on integer size (%lu) failed
 saving database definition
 saving encoding = %s
 saving large objects
 saving search_path = %s
 saving standard_conforming_strings = %s
 schema "%s" not found
 schema with OID %u does not exist
 select() failed: %s
 server version must be at least 7.3 to use schema selection switches
 server version: %s; %s version: %s
 skipping item %d %s %s
 skipping tar member %s
 sorter table "%s" could not be created, will not restore its data
 table "%s" not found
 tar archiver this format cannot be read
 transferring dependency %d -> %d to %d
 trigger "%s" not found
 unexpected COPY statement syntax: "%s"
 unexpected data offset flag %d
 unexpected policy command type: "%s"
 unexpected section code %d
 unexpected tgtype value: %d
 unrecognized archive format "%s"; please specify "c", "d", or "t"
 unrecognized command received from master: "%s"
 unrecognized constraint type: %c
 unrecognized data block type %d while restoring archive
 unrecognized data block type (%d) while searching archive
 unrecognized encoding "%s"
 unrecognized file format "%d"
 unrecognized object type in default privileges: %d
 unrecognized proparallel value for function "%s"
 unrecognized provolatile value for function "%s"
 unsupported version (%d.%d) in file header
 warning from original dump file: %s
 worker process failed: exit code %d
 wrote %lu byte of large object data (result = %lu)
 wrote %lu bytes of large object data (result = %lu)
 Project-Id-Version: pg_dump-tr
Report-Msgid-Bugs-To: pgsql-bugs@postgresql.org
POT-Creation-Date: 2018-04-20 21:53+0000
PO-Revision-Date: 2018-04-25 17:14+0300
Last-Translator: Devrim GÜNDÜZ <devrim@gunduz.org>
Language-Team: Turkish <devrim@gunduz.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=0;
X-Generator: Poedit 1.8.7.1
X-Poedit-Basepath: /home/ntufar/pg/pgsql/src/bin/pg_dump
X-Poedit-SearchPath-0: C:/pgsql/src/bin/pg_dump
X-Poedit-SearchPath-1: /home/ntufar/pg/pgsql/src/backend
X-Poedit-SearchPath-2: c:/pgsql/src/backend
 
Bağlantı Seçenekleri:
 
Genel seçenekler:
 
Eğer -f/--file kullanılmazsa, SQL betiği standart çıktıya
yazılacaktır.

 
Veritabanı adı verilmemişse PGDATABASE çevre değişkeni
kullanılacaktır.

 
Eğer giriş dosya adı verilmemişse, standart giriş akımı (stdin) kulanılacaktır.

 
Çıktı içeriğini kontrol eden seçenekler:
 
Geri güklemeyi kontrol eden seçenekler:
 
-I, -n, -P, -t, -T, ve --section seçenekleri, çoklu nesnelerin seçilmesi için
birden fazla kez birleştirilebilir ve belirtilebilir

   %s


   %s [SEÇENEK]...
   %s [SEÇENEK]... [VERİTABANI_ADI]
   %s [SEÇENEK]... [DOSYA]
   --binary-upgrade             sadece yükseltme araçlarının kullanımı için
   --column-inserts             veriyi kolon adları ile INSERT komutları olarak yedekle
   --disable-dollar-quoting     dollar quoting kullanmayı engelle, standart SQL quoting kullan
   --disable-triggers           salt-veri geri yüklemesi sırasında tetikleyicileri devre dışı bırak
   --enable-row-security        satır güvenliğini etkinleştir
   --enable-row-security        satır güvenliğini etkinleştir (sadece kullanıcının erişimi
                               olan içeriği yedekle)
   --exclude-table-data=TABLO   bu tablo veya tabloları yedekleme
   --if-exists                  nesneleri silerken IF EXISTS kullan
   --inserts                    veriyi COPY'den ziyade INSERT komutları olarak yedekle

   --lock-wait-timeout=ZAMANAŞIMI  tablo kilitlemesi için ZAMANAŞIMI kadar bekledikten sonra hata ver
   --no-data-for-failed-tables  oluşturulamayan tabloların verilerini geri
                               yükleme
   --no-security-labels         güvenlik etiketi atamalarını yedekleme
   --no-security-labels         güvenlik etiketlerini geri yükleme
   --no-synchronized-snapshots  paralel işlerde senkronize anlık görüntüleri yedekleme
   --no-tablespaces             tablespace atamalarını yedekleme
   --no-tablespaces             tablespace atamalarını geri yükleme
   --no-unlogged-table-data     loglanmayan tablo verisini yedekleme
   --quote-all-identifiers      anahtar sözcükler olmasa bile tüm belirteçleri çift tırnak içine al
   --role=ROL ADI             yedek işleminden önce SET ROLE çalıştır
   --role=ROL ADI          geri yüklemeden önce SET ROLE işlemini gerçekleştir
   --section=BÖLÜM            belirtilen bölümü yedekle (pre-data, data, veya post-data)
   --section=BÖLÜM            belirtilen bölümü yedekle (pre-data, data, veya post-data)
   --serializable-deferrable    yedeğin bir anormallik olmadan biteceği ana kadar bekle
   --snapshot=SNAPSHOT          yedek için verilen anlık görüntüyü (snapshot) kullan
   --use-set-session-authorization
                               Sahipliği ayarlamak için ALTER OWNER komutları yerine
                               SET SESSION AUTHORIZATION komutlarını kullan
   -1, --single-transaction     tek bir transaction olarak geri yükle
   -?, --help                   bu yardımı gösterir ve sonra çıkar
   -?, --help               bu yardımı gösterir ve sonra çıkar
   -C, --create                 hedef veritabanını oluştur
   -C, --create                 yedeğin (dump) içine veritabanını oluşturacak komutları da ekle
   -E, --encoding=DİLKODLAMASI      veriyi DİLKODLAMASI dil kodlamasıyla yedekle
   -F, --format=c|d|t       yedek dosya biçimi (otomatik olmalı)
   -F, --format=c|d|t|p         çıktı dosya biçimi (c:özel, d:dizin, t:tar,
                               p: düz metin (varsayılan))

   -I, --index=NAME             adı geçen indeksi geri yükle
   -L, --use-list=DOSYA ADI      çıktıyı seçmek/sıralamak için
                           bu dosyadaki içindekiler tablosunu kullan
   -N, --exclude-schema=ŞEMA  bu şema veya şemaları yedekleme
   -O, --no-owner              veri sahipliği ile ilgili bilgileri geri yükleme
   -O, --no-owner               düz metin biçiminde nesne 
                               sahipliğinin yüklenmesini atla
   -P, --function=NAME(args)    adı geçen fonksiyonu geri yükle
   -S, --superuser=NAME         triggerları devre dışı bırakmak için kullanılacak superuser kullanıcı adı
   -S, --superuser=İSİM         düz metin formatında kullanılacak superuser kullanıcı adı
   -S, --superuser=AD         yedeklerde kullanılacak superuser kullanıcı adı
   -T, --exclude-table=TABLO    ismi geçen tablo veya tabloları yedekleme
   -T, --trigger=NAME          adı geçen tetikleyiciyi geri yükle
   -U, --username=KULLANICI_ADI   bağlanılacak kullanıcı adı
   -V, --version                sürüm bilgisini göster, sonra çık
   -V, --version            sürüm bilgisini gösterir ve sonra çıkar
   -W              şifre sor (otomatik olarak her zaman açık)
   -Z, --compress=0-9           sıkıştırılmış biçimler için sıkıştırma seviyesi
   -a, --data-only              sadece veriyi yedekle (dump); şemayı yedekleme
   -a, --data-only              sadece veriyi geri yükle, şemaları değil
   -b, --blobs                  yedeğin (dump) içine büyük nesneleri dahil et
   -c, --clean                  veritabanı nesnelerini yeniden oluşturmadan önce temizle (kaldır)
   -c, --clean                  veritabanı nesnelerini yeniden oluşturmadan önce temizle (kaldır)
   -d, --dbname=CONNSTR     bağlantı cümleciğini kullanarak bağlan
   -d, --dbname=VERİTABANI_ADI      yedeklenecek veritabanı adı
   -d, --dbname=NAME        bağlanacak veritabanının adı
   -e, --exit-on-error          hata durumunda çık, varsayılan seçenek ise devam et
   -f, --file=DOSYA_ADI      çıktı dosya adı
   -f, --file=DOSYAADI          çıktı dosya adı ya da dizin adı
   -f, --file=DOSYA_ADI      çıktı dosya adı
   -g, --globals-only          sadece global nesneleri yedekle, veritabanlarını yedekleme
   -h, --host=HOSTNAME          veritabanı sunucusu adresi ya da soket dizini
   -j, --jobs=SAYI               döküm (dump) için belirtilen sayı kadar paralel süreç kullan
   -j, --jobs=SAYI               geri yükleme için belirtilen sayı kadar paralel süreç kullan
   -l, --database=VERİTABANI ADI    varsayılan alternatif veritabanı
   -l, --list               arşivin kısa içeriğini yaz
   -s, --schema=NAME            sadece bu şemaya ait nesneleri yükle
   -n, --schema=ŞEMA          sadece belirtilen şema veya şemaları yedekle
   -o, --oids                   yedeğin içine OID'leri de ekle
   -p PORT         veritabanı sunucusunun port numarası
   -r, --roles-only            sadece rolleri yedekle, veritabanlarını ya da tablespace'leri yedekleme
   -s, --schema-only            sadece şemayı yedekle (dump), veriyi değil
   -s, --schema-only            sadece şemayı yükle, veriyi yükleme
   -t, --table=NAME             adı geçen nesneyi (tablo, görünüm, vb.) geri yükle
   -t, --table=TABLO            sadece ismi geçen tablo veya tabloları yedekle
   -v, --verbose               detaylı açıklamalı mod
   -v, --verbose            verbose modu
   -w, --no-password        bağlanmak için kesinlikle parola sorma
   -x, --no-privileges          yetkileri yedekleme (grant/revoke)
   -x, --no-privileges          erişim haklarının geri yüklemesini atla (grant/revoke)
 %s %s veritabanını metin dosyası ya da diğer biçimlerde dump eder.

 %s, PostgreSQL veritabanı clusteri SQL betik dosyasına aktarıyor.

 %s, pg_dump tarafından oluşturulan PostgreSQL arşivinden veritabanı geri yükleniyor.

 %s: %s    Komut şuydu: %s

 %s: WSAStartup başarısız: %d
 %s: --single-transaction ve çoklu işi aynı anda belirtemezsiniz
 %s: "%s" veritabanına bağlanılamadı
 %s: "%s" veritabanına bağlanılamadı: %s
 %s: "postgres" veya "template1" veritabanına bağlanılamadı
Lütfen alternatif bir veritabanı belirtin
 %s: sunucu sürüm bilgisi alınamadı
 %s: "%s" çıktı dosyası açılamadı: %s
 %1$s: "%3$s" veritabanı için ACL (%2$s) listesi ayrıştırılamadı
 %1$s: "%3$s" tablespace için ACL (%2$s) listesi ayrıştırılamadı
 %s: "%s" sürüm bilgisi ayrıştırılamadı
 %s: "%s" çıktı dosyası yeniden açılamadı: %s
 %s: "%s" veritabanı aktarılıyor...
 %s: %s yürütülüyor
 %s: parallel iş sayısı geçersiz
 %s: azami paralel iş sayısı %d

 %s: --if-exists seçeneği -c/--clean seçeneğini gerektirir
 %s: options -c/--clean ve -a/--data-only seçenekleri aynı anda kullanılamazlar
 %s: -d/--dbname ve -f/--file seçenekleri birarada kullanılamazlar
 %s: -g/--globals-only ve -r/--roles-only seçenekleri beraber kullanılamaz
 %s: -g/--globals-only ve -t/--tablespaces-only seçenejleri beraber kullanılamaz
 %s: -r/--roles-only ve -t/--tablespaces-only seçenekleri birlikte kullanılamaz
 %s: options -s/--schema-only ve -a/--data-only seçenekleri aynı anda kullanılamazlar
 %s: pg_dump "%s" veritabanında başarısız oldu, çıkılıyor
 %s: sorgu başarısız oldu: %s %s: sorgu şu idi: %s
 %s: "pg_" ile başlayan rol adı atlandı (%s)
 %s: "%s" yürütülüyor
 %s: çok fazla komut satırı argümanı (ilki "%s" idi)
 %s: bilinmeyen bölüm adı "%s"
 (INSERT komutu OIDleri ayarlayamaz.)
 (Sistem kataloğu bozulmuş olabilir.)
 -C and -1 uyumsuz seçeneklerdir
 COPY "%s" tablosu için başarısız oldu: %s "%s" tablosunu içeriğinin aktarımı başarısız: PQgetCopyData() başarısız.
 "%s" tablosunu içeriğinin aktarımı başarısız: PQgetResult() başarısız.
 TOC girişte hata %d; %u %u %s %s %s
 Sunucudan hata mesajı alındı: %s FINALIZING sırasında hata:
 INITIALIZING sırasında hata:
 PROCESSING TOC sırasında hata:
 Bu sunucu sürümünde dışa aktarılmış anlık görüntü (exported snapshot) desteklenmemektedir.
 Şifre:  Hataları <pgsql-bugs@postgresql.org> adresine bildirin.
 Senkronize anlık görüntüler (snapshot) bu sunucu sürümü tarafından desteklenmiyor.
Senkronize anlık görüntülere ihtiyaç yoksa bunun yerine --no-synchronized-snapshots 
ile çalıştırın.
 Yedek (standby) sunucularda anlık görüntüler (snapshot) bu sunucu sürümünde desteklenmiyor.
Senkronize anlık görüntülere ihtiyaç yoksa bunun yerine --no-synchronized-snapshots
ile çalıştırın.
 %2$s adresinde %1$s TOC Girişi (uzunluk %3$s, sağlama toplamı %4$d)
 O sırada yürütülen komut: %s
 "pg_dump" uygulaması %s için gerekmektedir ancak
"%s" ile aynı dizinde bulunamadı.
Kurulumunuzu kontrol edin.
 "pg_dump" uygulaması "%s" tarafından bulundu
ancak %s ile aynı sürüm değildir.
Kurulumunuzu kontrol edin.
 Daha fazla bilgi için "%s --help" yazabilirsiniz.
 Kullanımı:
 UYARI: %s aggregate fonksiyonu veritabanın bu sürümünde düzgün dump edilemiyor; atlanıyor
 UYARI: arşiv sıkıştırılmıştır, ancak bu kurulum sıkıştırmayı desteklemiyor -- veri kaydedilmeyecek
 UYARI: arşiv öğeleri doğru bölüm sırasında değil
 UYARI: arşıv doyası daha büyük integer sayılarına sahip platformda yapılmış, bazı işlemler başarısız olabilir
 UYARI: belirsiz dönüşüm tanımı, trffromsql ve trftosql'in en azından biri sıfırdan farklı olmalı
 UYARI: pg_cast.castmethod field alanı içinde belirsiz değer
 UYARI: pg_cast.castmethod field alanı içinde belirsiz değer
 UYARI: pg_transform.trffromsql alanı içinde beklenmeyen değer
 UYARI: pg_transform.trftosql alanı içinde beklenmeyen değer
 UYARI: proargnames dizisi içinde beklenmeyen değer
 UYARI: OID %s olan operatör bulunamadı
 UYARI: "%s" ifadesinde nereye IF EXISTS ekleneceği bulunamadı

 UYARI: proallargtypes dizisi ayrıştırılamadı
 UYARI: proargmodes dizisi ayrıştırılamadı
 UYARI: proargnames dizisi ayrıştırılamadı
 UYARI: proconfig dizisi ayrıştırılamadı
 UYARI: reloptions dizisi (array) ayrıştırılamadı
 UAYRI: "%s" nesne tipi için sahip bilgisinin nasıl ayarlanacağı bilinmiyor
 UYARI: yükleme sırasında hata es geçildi: %d
 WARNING: ftell fonksiyonun bidirdiği pozisyonu ile beklenen pozisyon uyumsuz -- ftell kullanıldı
 UTAYI: veri başlığında geçersiz tarih
 UYARI: "%2$s" erişim yöntemi için geçersiz tip "%1$c"
 UYARI: satır yoksayıldı: %s
 UYARI: "%s" aggregate fonksiyonun sahibi geçersizdir
 UYARI: "%s" veri tipinin sahibi geçersizdir
 UYARI: "%s" fonksiyonunun sahibi geçersizdir
 UYARI: "%s" operatörün  sahibi geçersizdir
 UYARI: "%s" operator sınıfının sahibi geçersizdir
 UYARI: "%s" operatör ailesinin sahibi geçersizdir
 UYARI: "%s" şemasının sahibi geçersizdir
 UYARI: "%s" tablosunun sahibi geçersizdir
 UYARI: bu kurulumda sıkıştırma desteklenmemektedir -- arşiv sıkıştırılmayacak
 UYARI: "%s" veri tipinin typtype'i geçersiz görünüyor
 WARNING: "%s" tablosunun COPY işlemi sırasında beklenmeyen ilave sonuçlar
 alt süreç beklenmeyen biçimde sonlandı
 sunucu sürümü uyuşmazlığına rağmen devam ediliyor
 gerçek dosya uzunluğu (%s) beklenen uzunluğu (%s) ile uyuşmamaktadır
 %s için AH ayırılıyor, biçim %d
 bir veritabanına zaten bağlı
 archiver archiver (db) arşiv formatı doğrulanmaya çalışılıyor
 kötü dumpId
 TABLE DATA öğesi için kötü tablo dumpId'si
 Sadece girdi arşivleri tekrar açılabilir
 null pointer duplicate edilemiyor (iç hata)
 sıkıştırılmış arşivden yükleme başarısız (bu kurulumda sıkıştırma desteklenmiyor)
 alt süreç %d çıkış koduyla sonuçlandırılmıştır alt süreç %d bilinmeyen durumu ile sonlandırılmıştır alt süreç 0x%X exception tarafından sonlandırılmıştır alt süreç %d sinyali tarafından sonlandırılmıştır alt süreç %s sinyali tarafından sonlandırılmıştır komut çalıştırılabilir değil komut bulunamadı compress_io sıkıştırma, tar çıktı formatı tarafından desteklenmiyor
 sıkıştırma seviyesi 0..9 aralığında olmalı
 sıkıştırma etkin
 "%2$s" kullanıcısı olarak "%1$s" veritabanına bağlanıldı
 geri yüklemek için veritabana bağlanılıyor
 "%s" veritabanına bağlanılıyor
 bağlantı parola gerektiriyor 
 "%s" veritabanına bağlantı başarısız oldu: %s %s dosyasında bozuk tar başlığı (beklenen: %d, hesaplanan: %d) dosya pozisyonu %s
 çalışma dizini "%s" olarak değiştirilemedi: %s TOC dosyası kapatılamıyor: %s
 arşiv dosyası kapatma hatası: %s
 sıkıştırma kütüphanesi kapatılamadı: %s
 sıkıştırma akımı kapatılamadı: %s
 veri dosyası kapatılamadı: %s
 "%s" dizini kapatılamadı: %s
 çıktı dosyası kapatılamadı: %s
 "%s" large object TOC dosyası kapatılamadı: %s
 çıktı dosyası kapatılamadı: %s
 tar öğesi kapatılamadı
 geçici dosya kapatma hatası: %s
 Veritabanı transaction'u commit edilemedi veri sıkıştırılamadı: %s
 iletişim kanalları oluşturulamadı: %s
 "%s" dizini oluşturulamadı: %s
 %u large object oluşturulamadı: %s işçi süreci yaratılamadı: %s
 arşiv dosyasınde arama pozisyonu belirlenemedi: %s
 sorgu çalıştırılamadı "%s"  çalıştırmak için bulunamadı %d blok ID'si arşivde bulunamadı -- arşiv bozulmuş olabilir
 arşivde %d block ID'si bulunamadı -- arşivdeki eksik veri konumu nedeniyle işlenemeyen geçersiz yükleme isteği nedeniyle olabilir
 arşivde %d block ID'si bulunamadı -- aranamayan girdi dosyası nedeniyle işlenemeyen geçersiz yükleme isteği nedeniyle olabilir
 ID %d için bir alan girdisi bulunamıyor
 "%s" dosyası arşivde bulunamadı
 OID %u olan fonksiyon için fonksiyon tanımı bulunamadı
 tar arşivinde "%s" dosyası için başlık bulunamadı
 %s için üst uzantı bulunamadı %s
 bitmiş sürecin yuvasu bulunamadı
 geçici dosya adı oluşturulamadı: %s
 libpq kütüphanesinden server_version alınamadı
 geçerli dizin tespit edilemedi: %s bağımlılık döngüsü tespit edilemedi
 sıkıştırma kütüphanesi ilklendirilemedi: %s
 "%s" nesnesi üzerinde kilit alınamadı
Bu genellikle pg_dump ana süreci tablo üzerinde başta ACCESS SHARE kilidi aldıktan sonra başka birinin tablo üzerinde ACCESS EXCLUSIVE kilidi talep ettiği anlamına gelir.
 "%s" TOC dosyası girdi için açılamadı: %s
 çıktı için "%s" TOC dosyası açılamadı: %s
 "%s" TOC dosyası açılamadı: %s
 girdi için TOC dosyası açılamadı: %s
 çıktı için TOC dosyası açılamadı: %s
 "%s" giriş dosyası açılamadı: %s
 giriş dosyası açılamadı: %s
  %u large object açılamadı: %s girdi için "%s" large object TOC dosyası açılamadı: %s
 "%s" çıktı dosyası açılamadı: %s
 çıktı dosyası açılamadı: %s
 geçici dosya açılamadı 
 öntanımlı ACL listesi ayrıştırılamıyor (%s)
 "%s" numerik dizisi ayrıştırılamadı: sayıda geçersiz karakter
 "%s" numerik dizisi ayrıştırılamadı: çok fazla sayı
 current_schemas() sonucu ayrıştırılamıyor
 "%s" ikili (binary) dosyası okunamadı "%s" dizini okunamıyor: %s
 giriş dosyası okuma hatası: %s
 giriş dosyası okuma hatası: dosya sonu
 giriş dosyası okuma hatası: %s
 symbolic link "%s" okuma hatası %s veritabanına yeniden bağlanılamadı default_tablespace %s olarak değiştirilemedi: %s default_with_oids ayarlanamıyor: %s search_path "%s" olarak değiştirilemedi: %s arşiv dosyasında arama pozisyonu ayarlanamadı: %s
 oturum kullanıcısını "%s" olarak değiştirilemedi: %s ;veritabanı transaction'u başlatılamadı sıkıştırılmış veri açılamadı: %s
 blobs TOC dosyası yazma hatası
 large-object yazılamıyor (sonuç: %lu, beklenen: %lu)
 çıktı dosyasına yazma başarısız: %s
 iletişim kanalına yazma başarısız: %s
 %s oluşturuluyor "%s"
 %s oluşturuluyor "%s.%s"
 custom archiver "%s" vew tanımı boştur (uzunluk sıfır)
 dosya başlığında kod satırı blunamadı
 1.3 sürüm öncesi arşivlerinde doğrudan veritabanı bağlantıları desteklenmemektedir
 "%s" dizini geçerli bir arşiv olarak görünmüyor ("toc.dat" bulunamadı)
 directory archiver dizin adı çok uzun:: "%s"
 %s nesnesinin tetikleyicileri etkisiz hale getiriliyor
 %s %s kaldırılıyor
 "%s.%s" tablosunun içeriği yedekleniyor
 %s nesnesinin tetikleyicileri etkineştiriliyor
 ana paralel döngüye giriyor
 restore_toc_entries_parallel'e giriliyor
 restore_toc_entries_postfork'a giriliyor
 restore_toc_entries_prefork'a giriliyor
 ID %d olan giriş kapsam dışıdır -- bozuk TOC olabilir
 yedek alma sırasında hata oluştu
 dosya içerisinde gösterge ilerleme hatası: %s
 bir paralel iş öğesinin işlenmesinde hata oluştu
 %u large object okurken hata oldu: %s "%s" large object TOC dosyası okuma hatası
 PQputCopyData'nın döndürdüğü hata: %s PQputCopyEnd'in döndürdüğü hata: %s %s %s yürütülüyor
 %d check kısıtlamasının "%s" tablosunda bulunması beklendi; ancak  %d bulundu
 %d check kısıtlamasının "%s" tablosunda bulunması beklendi; ancak  %d bulundu
 dosyada bulunan biçim (%2$d) beklenen biçimden (%1$d) farklıdır
 tutarlılık kontrolü başarısız, "%2$s" tablosunun (OID %3$u) üst OID %1$u bulunamadı
 tutarlılık kontrolü başarısız, üst tablo OID'inin %u, pg_rewrite girdisi OID'i %u bulunamadı
 tutarlılık kontrolü başarısız, %2$u OID'li dizinin (sequence) %1$u OID'li üst tablosu bulunamadı
 veritabanına bağlantı başarısız oldu
 veritabana yeniden bağlanma hatası
 dosya adı çok uzun: "%s"
 dump dosyasında dosya göstergesi çok büyük
 "%s.%s" tablosu için kontrol kısıtlamaları bulunuyor
 "%s.%s" tablosu için varsayılan ifadeler aranıyor
 uzantı tabloları bulunuyor
 inheritance ilişkiler bulunuyor
 "%s.%s" tablosunun sütunları ve tipleri bulunuyor
 %d %s %s öğesi bitirildi
 ana paralel döngü bitti
 alt tablolarında inherited sütunlar işaretleniyor
 veriyi okurken beklenmeyen blok ID (%d) bulundu -- beklenen: %d
 "%s" fonksiyonu bulunamadı
 uzantı üyeleri belirleniyor
 örtük salt veri geri yükleme
 Eksik tar başlığı bulundu (%lu byte)
 Eksik tar başlığı bulundu (%lu byte)
 "%s" indeksi bulunamadı
 girdi dosyası metin biçiminde bir döküm (dump) gibi görünüyor. Lütfen psql kullanın.
 girdi geçerli bir arşiv değildir
 giriş, geçerli bir arşiv değildir (çok kısa?)
 giriş fazla kısa (okunan: %lu, beklenen: 5)
 İç hata - WriteData cannot be called outside the context of a DataDumper routine
 iç hata - th ya da fh, tarReadRaw() içinde belirtilmedi
 geçersiz ENCODING öğesi: %s
 large object için geçersiz OID
 (%u) large objecti için geçersiz OID
 geçersiz STDSTRINGS öğesi: %s
 "%2$s" tablosu için geçersiz adnum değeri %1$d
 "%3$s" tablosunun "%2$s" tetikleyicisi için geçersiz satır argümanı (%1$s)
 geçersiz ikili (binary) "%s" belirtilen "%s" istemci dil kodlaması geçersiz
 "%2$s" tablosu için geçersiz sütun numarası %1$d
 "%s" tablosunda geçersiz kolon numaralandırlması
 geçersiz sıkıştırma kodu: %d
 geçersiz bağımlılık %d
 geçersiz dumpId %d
 "%s" large object TOC dosyasında geçersiz satır: %s
 alt süreçten (worker) geçersiz mesaj alındı: "%s"
 parallel iş sayısı geçersiz
 Geçersiz çıktı biçimi belirtildi: "%s" 
 seçilen biçimde large-object çıktısı desteklenememektedir
 Son gömülü OID : %u
 %d %s %s öğesi başlatılıyor
 "%s" bütünlük kısıtlamasının indexi eksik
 dosya içerisinde %s yerinden bir sonraki %s yerine geçiş yapılamıyor
 hiç bir öğe hazır değil
 uygun şema bulunamadı
 "%s" şablonu (pattern) için eşleşen şema bulunamadı
 uygun tablo bulunamadı
 "%s" şablonu (pattern) için eşleşen tablo bulunamadı

 herhangi bir çıktı dizini belirtilmedi
 zlib desteği ile build edilmemiş
 şu an dosyanın %s yerinde
 --if-exists seçeneği -c/--clean seçeneğini gerektirir
 --inserts/--column-inserts ve -o/--oids beraber kullanılamazlar
 options -c/--clean ve -a/--data-only seçenekleri aynı anda kullanılamazlar
 options -s/--schema-only ve -a/--data-only seçenekleri aynı anda kullanılamazlar
 yetersiz bellek
 on_exit_nicely slotları yetersiz
 paralel arşivleyici paralel yedek sadece dizin biçimi tarafından destekleniyor
 taranamayan dosyadan paralel geri yükleme desteklenmiyor
 standart girdiden paralel geri yükleme desteklenmiyor 
 paralel geri yükleme özelliği 8.0 öncesi pg_dump ile yapılan arşivleri desteklememektedir
 paralel geri yükleme bu arşiv biçimini desteklemiyor
 pclose başarısız oldu: %s pgpipe: bağlantı kabul edilemedi: hata kodu %d
 pgpipe: bağlanamadı (bind): hata kodu %d
 pgpipe: soket bağlanamadı: hata kodu %d
 pgpipe: ikinci soket oluşturulamadı: hata kodu %d
 pgpipe: socket oluşturulamadı: hata kodu %d
 pgpipe: dinleyemedi: hata kodu %d
 pgpipe: getsockname() başarısız oldu: çıkış kodu %d
 %s işleniyor
 "%s.%s" tablosu için veri işleniyor
 %d %s %s öğesi işleniyor
 atlanan %d %s %s öğesi işleniyor
 sorgu başarısız oldu: %s "%2$s" tablosu üzerindeki "%1$s" foreign key tetikleyici için sorgu, null referans edilen tablo sayısı getirdi (tablo OID: %3$u)
 sorgu 1 yerine %d satır döndürdü: %s
 sorgu 1 yerine %d satır döndürdü: %s
 "%s" sequence verisini getirecek sorgu %d satır döndürdü (bir satır bekleniyordu)
 "%s" sequence verisini getirecek sorgu %d satır döndürdü (bir satır bekleniyordu)
 "%s" sequence verisini getirecek sorgu "%s" adını getirdi
 "%s" tablosundan "%s" rule'unu getiren sorgu başarısız: yanlış satır sayısı döndürüldü
 "%s" vew tanımını getirecek sorgu birden çık tanımı getirdi
 "%s" vew tanımını getirecek sorgu hiçbir veri getirmedi
 sorgu şu idi: %s
 %3$s %4$s için TOC öğe %1$d (ID %2$d) okunuyor
 ilgili tabloların sütun bilgisi okunuyor
 bütünlük kısıtlamaları okunuyor
 öntanımlı yetkiler okunuyor
 bağımlılık verileri okunuyor
 olay tetikleyicileri okunuyor
 uzantılar okunuyor
 "%s.%s" tablosunun foreign key bütünlük kısıtlamaları okunuyor
 indexler okunuyor
 "%s.%s" tablosunun indeksleri okunuyor
 large objectler okunuyor
 ilkeler okunuyor
 "%s.%s" tablosu için ilkeler (policy) okunuyor
 yordamsal diller okunuyor
 rewrite ruleler okunuyor
 "%s.%s" tablosu için etkinleştirilen satır güvenliği (row security) okunuyor
 şemalar okunuyor
 kullanıcı tanımlı inheritance bilgisi okunuyor
 dönüşümler okunuyor
 tetikleyiciler okunuyor
 "%s.%s" tablosunun tetikleyicileri okunuyor
 type castlar okunuyor
 kullanıcı tanımlı erişim yöntemleri okunuyor
 kullanıcı-tanımlı aggregate fonksiyonlar okunuyor
 kullanıcı tanımlı collationlar okunuyor
 kullanıcı tanımlı dönüşümler okunuyor
 kullanıcı tanımlı foreign sunucular okunuyor
 kullanıcı tanımlı foreign-data wrapperlar okunuyor
 kullanıcı tanımlı fonksiyonlar okunuyor
 kullanıcı-tanımlı operatör sınıfları okunuyor
 kullanıcı tanımlı operatör aileleri okunuyor
 kullanıcı tanımlı operatörler okunuyor
 kullanıcı tanımlı tablolar okunuyor
 kullanıcı-tanımlı metin arama yapılandırmaları okunuyor
 kullanıcı-tanımlı metin arama sözlükleri okunuyor
 kullanıcı tanımlı metin arama ayrıştırıcıları okunuyor
 kullanıcı tanımlı metin arama şablonları okunuyor
 kullanıcı tanımlı tipler okunuyor
  %d için bağımlılıklar azaltılıyor
 %d large object geri yüklendi
 %d large object geri yüklendi
 bu arşiv biçinide sıra dışı veri geri yüklemesi desteklenmemektedir: "%s" bekleniyor ancak arşiv dosyasında %s ondan önce gelmektedir.
 large-object OID %u geri yükleniyor
 integer boyutu (%lu) turtarlılık kontrolü başarısız
 veritabanın tanımı kaydediliyor
 dil kodlaması = %s
 large objectler kaydediliyor
 search_path = %s olarak kaydediliyor

 kaydedilen: standard_conforming_strings = %s
 "%s" şeması bulunamadı
 OID %u olan şema mevcut değil
 select() başarısız oldu: %s
 şema seçim anahtarlarını kullanmak için sunucu sürümü 7.3 ya da daha yüksek olmalıdır
 sunucu sürümü: %s; %s sürümü: %s
 %d %s %s öğesi atlanıyor
 %s tar öğesi atlandı
 sorter "%s" tablosu oluşturulamadı, onun verileri yüklenmeyecektir
 "%s" tablosu bulunamadı
 tar archiver bu biçim okunamıyor
 %d -> %d bağımlılığı %d olarak aktarılıyor
 "%s" tetikleyicisi bulunamadı
 beklenmeyen COPY ifadesi söz dizimi: "%s"
 beklenmeyen veri konum bayrağı %d
 beklenmeyen ilke (policy) komut türü: "%s"
 beklenmeyen bölüm kodu %d
 beklenmeyen tgtype değeri: %d
 tanımlanamayan arşiv formatı "%s"; lütfen "c", "d", ya da "t" seçeneklerinden birisini belirtiniz.
 ana sunucudan (master) bilinmeyen komut alındı: "%s"
 bilinmeyen bütünlük kısıtlama türü: %c
 arşivi yüklerken bilinmeyen veri blok tipi %d bulundu
 arşivde ararken tanınmayan veri tipine (%d) rastlandı
 tanınmayan dil kodlaması: "%s"
 tanınmayan dosya biçimi: "%d"
 öntanımlı yetkilerde bilinmeyen nesne tipi: %d
 "%s" fonksiyonu için bilinmeyen proparalel değeri
 "%s" fonksiyonu için bilinmeyen provolatile değeri
 dosya başlığında desteklenmeyen sürüm (%d.%d)
 asıl dump dosyasından uyarı: %s
 alt süreç başarısız oldu: çıkış kodu %d
 large object verisinin %lu baytı yazıldı (sonuç = %lu)
 large object verisinin %lu baytı yazıldı (sonuç = %lu)
 